#!/usr/bin/perl

use strict ;
use warnings ;

my $overwrite ;#= 1 ;

my %hadthat ;
my $dump_dir = '/shared/dumps' ;

if ( -d $dump_dir ) {
	foreach my $f ( `ls $dump_dir` ) {
		chomp $f ;
		next unless $f =~ m/(20\d{6}).json.gz/ ;
		process ( $1 , "$dump_dir/$f" ) ;
	}
}

0 ;

sub process {
	my ( $date , $fn ) = @_ ;
	return if defined $hadthat{$date} ;
	$hadthat{$date} = 1 ;
	return unless -e $fn ;
	my $outfile = "/data/project/wikidata-todo/stats/data/$date.json" ;
	return if -e $outfile and not defined $overwrite ;
	my $cmd = "/data/project/wikidata-todo/stats/job.sh $date $fn" ;
#	print "$cmd\n" ;
	`$cmd` ;
}
