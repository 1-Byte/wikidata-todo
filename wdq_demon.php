#!/usr/bin/php
<?PHP

ini_set('memory_limit','1500M');
set_time_limit ( 60 * 10 ) ; // Seconds

require_once ( 'public_html/php/common.php' ) ;

$db = openToolDB ( 'feed_p' ) ;
$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
$ts_now = date ( "YmdHis" ) ;

function updateFeed ( $feed ) {
	global $db , $dbwd , $ts_now , $wdq_internal_url ;
	
	# Cet the current items for the query
	$wdq = json_decode ( file_get_contents ( "$wdq_internal_url?q=".urlencode($feed->wdq) ) ) ;
	if ( !isset($wdq->items) ) return ; // TODO error logging

	# Get the last state
	$db = openToolDB ( 'feed_p' ) ;
	$iid = array() ;
	$sql = "SELECT * FROM item_cache WHERE feed=" . $feed->id ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$iid[$o->item] = $o ;
	}
	
	$items_new = array() ;
	$items_removed = array() ;
	$items_updated = array() ;
	
	foreach ( $wdq->items AS $i ) {
		if ( isset($iid[$i]) ) $iid[$i]->check = true ;
		else $items_new[] = "Q$i" ;
	}
	
	foreach ( $iid AS $item => $o ) {
		if ( $o->last_revision == 0 ) continue ; // Not-in-results marker
		if ( !isset($o->check) ) $items_removed[] = $item ;
	}
	
/*
	$sql = '' ;
	foreach ( $iid AS $item => $o ) {
		if ( !isset($o->check) ) continue ;
		if ( $o->last_revision == 0 ) continue ; // Not-in-results marker
		if ( $sql != '' ) $sql .= " OR " ;
		$sql .= "(page_title='Q$item' AND page_latest!=" . $o->last_revision . ")";
	}
	$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
	if ( $sql != '' ) {
		$sql = "SELECT page_title,page_latest,page_touched FROM page WHERE page_namespace=0 AND ($sql)" ;
		if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']'."\n$sql\n\n");
		while($o = $result->fetch_object()){
			$sql = "UPDATE item_cache SET last_revision=" . $o->page_latest . ",event='Item was updated',timestamp='" . $o->page_touched . "' WHERE feed=" . $feed->id . " AND item=" . preg_replace('/\D/','',$o->page_title) ;
			if(!$r2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
		}
	}
*/

	$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
	foreach ( $iid AS $item => $o ) {
		if ( !isset($o->check) ) continue ;
		if ( $o->last_revision == 0 ) continue ; // Not-in-results marker
		$sql = "SELECT page_title,page_latest,page_touched FROM page WHERE page_namespace=0 AND page_title='Q$item' AND page_latest!=" . $o->last_revision ;
		if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']'."\n$sql\n\n");
		while($o = $result->fetch_object()){
			$sql = "UPDATE item_cache SET last_revision=" . $o->page_latest . ",event='Item was updated',timestamp='" . $o->page_touched . "' WHERE feed=" . $feed->id . " AND item=" . preg_replace('/\D/','',$o->page_title) ;
			if(!$r2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
		}
	}





	
	if ( count($items_new) > 0 ) {
		$sql = "SELECT page_title,page_latest FROM page WHERE page_namespace=0 AND page_title IN ('" . implode("','",$items_new) . "')" ;
		if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']'."\n$sql\n\n");
		while($o = $result->fetch_object()){
			$sql = "INSERT IGNORE INTO item_cache (feed,item,last_revision,timestamp,event) VALUES (" . $feed->id . "," . preg_replace('/\D/','',$o->page_title) . "," . $o->page_latest . ",'$ts_now','New in query results')" ;
			if(!$r2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
		}
	}
	
	if ( count($items_removed) > 0 ) {
		$sql = "UPDATE item_cache SET timestamp='$ts_now',event='Not in query results anymore',last_revision=0 WHERE feed=" . $feed->id . " AND item IN (" . implode(',',$items_removed) . ")" ;
		if(!$r2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	}

	$sql = "UPDATE feed SET last_update='$ts_now' WHERE id=" . $feed->id ;
	if(!$r2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	
}



$feeds = array() ;
$sql = "SELECT * FROM feed" ;
if ( isset ( $argv[1] ) ) $sql .= " WHERE id=" . ($argv[1]*1) ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	$feeds[$o->id] = $o ;
}

foreach ( $feeds AS $feed_id => $feed ) {
	updateFeed ( $feed ) ;
}


?>