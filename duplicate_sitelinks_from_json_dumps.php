#!/usr/bin/php
<?PHP

// zcat /shared/dumps/20141027.json.gz | ./duplicate_sitelinks_from_json_dumps.php | sort | gzip -c > sitelinks.20141027.gz

while ( !feof(STDIN) ) {
	$line = trim(fgets(STDIN));
	if ( !preg_match ( '/^\{/' , $line ) ) continue ;
	$line = preg_replace ( '/,$/' , '' , $line ) ;
	$j = json_decode ( $line ) ;
	if ( !isset($j->sitelinks) ) continue ;
	$q = $j->id ;
	foreach ( $j->sitelinks AS $s ) {
		print $s->site . ':' . $s->title . "\t$q\n" ;
	}
}

?>