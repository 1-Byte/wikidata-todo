<?PHP

include_once ( '../public_html/php/common.php' ) ;

$gsdata = array() ;



function getTemplate ( $template , $w ) {
	if ( false === preg_match ( "/\{\{$template(.+?)\}\}/i" , str_replace("\n"," ",$w) , $m ) ) return "" ;
	if ( $m[1] == '' ) return "" ;
	$ret = $m[1] ;
	if ( preg_match ( "/^[^|]+(.*)$/" , $ret , $m ) ) $ret = $m[1] ;
	return $ret." |" ;
}

function padStringWithZeros ( $d , $num ) {
	while ( strlen($d) < $num ) $d = '0'.$d ;
	return $d ;
}

function escapeStringValue ( $val ) {
	return '"'.str_replace('"','\"',$val).'"' ;
}

function setStringProp ( $prop , $val ) {
	global $gsdata ;
	$val = preg_replace ( '/<!--(.*?)-->/' , '' , $val ) ; // HTML comments
	$val = trim ( $val ) ;
	if ( $val == '' ) return ;
	$gsdata['out'][$prop][] = array ( escapeStringValue($val) , 'S143' , $gsdata['imported_from'] ) ;
}

function setItemProp ( $prop , $val ) {
	global $gsdata ;
	$val = trim ( $val ) ;
	if ( $val == '' ) return ;
	$gsdata['out'][$prop][] = array ( $val , 'S143' , $gsdata['imported_from'] ) ;
}

function setStringQualProp ( $prop , $item , $qprop , $val ) {
	global $gsdata ;
	$val = trim ( $val ) ;
	if ( $val == '' ) return ;
	$gsdata['out'][$prop][] = array ( $item , $qprop , escapeStringValue($val) , 'S143' , $gsdata['imported_from'] ) ;
}

function setDateProp ( $prop , $val ) { // +00000001967-08-19T00:00:00Z | precision: 11=day, 10=month, 9=year
	global $gsdata ;
	$val = preg_replace ( '/\[\[/' , '' , $val ) ;
	$val = preg_replace ( '/\]\]/' , '' , $val ) ;
	$val = preg_replace ( '/<ref.+?<\/ref>/' , '' , $val ) ;
	$val = preg_replace ( '/<ref.+?\/>/' , '' , $val ) ;
	$val = preg_replace ( '/\s*(aged{0,1} \d+)\s*/' , '' , $val ) ;
	$val = preg_replace ( '/\s*,{0,1}\s*\(\)\s*/' , '' , $val ) ;
	
	$val = trim ( $val ) ;
	if ( $val == '' ) return ;

	if ( preg_match ( '/^\s*(\d{3,4})-(\d{1,2})-(\d{1,2})\s*$/' , $val , $m ) ) {
		$year  = padStringWithZeros($m[1],4);
		$month = padStringWithZeros($m[2],2);
		$day   = padStringWithZeros($m[3],2);
		if ( $month*1 > 12 ) return ;
		$gsdata['out'][$prop][] =  array ( "+0000000$year-$month-$day"."T00:00:00Z/11" , 'S143' , $gsdata['imported_from'] ) ;
		
	} else if ( preg_match ( '/^\s*(\d{1,2})[.thrdnd]{0,2} (.+) (\d{3,4})\s*$/' , $val , $m ) ) { // 12th April 1953
		$m[2] = ucfirst($m[2]) ;
		if ( !isset($gsdata['months'][$m[2]]) ) { // No such month
			fwrite(STDERR, "Q".$gsdata['q']." : No such month '" . $m[2] . "'\n");
			return ;
		}
		$year  = padStringWithZeros($m[3],4);
		$month = padStringWithZeros($gsdata['months'][$m[2]],2);
		$day   = padStringWithZeros($m[1],2);
		if ( $month*1 > 12 ) return ;
		$gsdata['out'][$prop][] =  array ( "+0000000$year-$month-$day"."T00:00:00Z/11" , 'S143' , $gsdata['imported_from'] ) ;

	} else if ( preg_match ( '/^\s*(\D+?) (\d{1,2})(\S{0,2}),{0,1} (\d{3,4})\s*$/' , $val , $m ) ) { // October 23, 1953
		$m[1] = ucfirst($m[1]) ;
		if ( !isset($gsdata['months'][$m[1]]) ) { // No such month
			fwrite(STDERR, "Q".$gsdata['q']." : No such month '" . $m[1] . "'\n");
			return ;
		}
		$year  = padStringWithZeros($m[4],4);
		$month = padStringWithZeros($gsdata['months'][$m[1]],2);
		$day   = padStringWithZeros($m[2],2);
		if ( $month*1 > 12 ) return ;
		$gsdata['out'][$prop][] =  array ( "+0000000$year-$month-$day"."T00:00:00Z/11" , 'S143' , $gsdata['imported_from'] ) ;
		
	} else if ( preg_match ( '/^\s*(\d{1,2}\S*) (Jahrhundert|century)\s*$/' , $val , $m ) ) { // 15th century
		$year = padStringWithZeros($m[1]*100,4);
		if ( $year * 1 == 0 ) return ;
		$gsdata['out'][$prop][] =  array ( "+0000000$year-01-01"."T00:00:00Z/7" , 'S143' , $gsdata['imported_from'] ) ;
		
	} else if ( preg_match ( '/^\s*([A-Z].+?) (\d{3,4})\s*$/' , $val , $m ) ) { // Juli 1964
		$m[1] = ucfirst($m[1]) ;
		if ( !isset($gsdata['months'][$m[1]]) ) { // No such month
			fwrite(STDERR, "Q".$gsdata['q']." : No such month '" . $m[1] . "'\n");
			return ;
		}
		$year  = padStringWithZeros($m[2],4);
		$month = padStringWithZeros($gsdata['months'][$m[1]],2);
		if ( $month*1 > 12 ) return ;
		$gsdata['out'][$prop][] =  array ( "+0000000$year-$month-01"."T00:00:00Z/10" , 'S143' , $gsdata['imported_from'] ) ;
		
	} else if ( preg_match ( '/^\s*(\d{3,4})\s*$/' , $val , $m ) ) {
		$year = padStringWithZeros($m[1],4);
		if ( $year * 1 == 0 ) return ;
		$gsdata['out'][$prop][] =  array ( "+0000000$year-01-01"."T00:00:00Z/9" , 'S143' , $gsdata['imported_from'] ) ;
		
	} else if ( preg_match ( '/^\s*(\d{2,4})\s*B\.{0,1}C\.{0,1}E{0,1}\.{0,1}$/' , $val , $m ) ) {
		$year = padStringWithZeros($m[1],4);
		if ( $year * 1 == 0 ) return ;
		$gsdata['out'][$prop][] =  array ( "-0000000$year-01-01"."T00:00:00Z/9" , 'S143' , $gsdata['imported_from'] ) ;
		
	} else {
		fwrite(STDERR, "Q".$gsdata['q'].": Can't parse date '$val'\n");
	}
}

function setLCCN ( $s ) {
	$s = str_replace('/','',$s) ;
	if ( preg_match ( '/^(n[a-z])([3-9]\d)(\d+)$/' , $s , $m ) ) {
		$s = $m[1] . $m[2] . padStringWithZeros ( $m[3] , 6 ) ;
	} else if ( preg_match ( '/^(n[a-z])(2\d\d\d)(\d+)$/' , $s , $m ) ) {
		$s = $m[1] . $m[2] . padStringWithZeros ( $m[3] , 6 ) ;
	} else return ;
	setStringProp ( 'P244' , $s ) ;
}


function checkAC ( $ac ) {
	if ( preg_match ( '/\|\s*GND\s*=\s*([^|]+)\s*\|/' , $ac , $m ) ) setStringProp ( 'P227' , $m[1] ) ;
	if ( preg_match ( '/\|\s*VIAF\s*=\s*([^|]+)\s*\|/' , $ac , $m ) ) setStringProp ( 'P214' , $m[1] ) ;
	if ( preg_match ( '/\|\s*LCCN\s*=\s*([^|]+)\s*\|/' , $ac , $m ) ) setLCCN ( $m[1] ) ;
	if ( preg_match ( '/\|\s*NDL\s*=\s*([^|]+)\s*\|/' , $ac , $m ) ) setStringProp ( 'P349' , $m[1] ) ;
	if ( preg_match ( '/\|\s*ORCID\s*=\s*([^|]+)\s*\|/' , $ac , $m ) ) setStringProp ( 'P496' , $m[1] ) ;
	if ( preg_match ( '/\|\s*ISNI\s*=\s*([^|]+)\s*\|/' , $ac , $m ) ) setStringProp ( 'P213' , $m[1] ) ;
	if ( preg_match ( '/\|\s*SELIBR\s*=\s*([^|]+)\s*\|/' , $ac , $m ) ) setStringProp ( 'P906' , $m[1] ) ;
	if ( preg_match ( '/\|\s*BNF\s*=\s*([^|]+)\s*\|/' , $ac , $m ) ) setStringProp ( 'P268' , $m[1] ) ;
	if ( preg_match ( '/\|\s*BPN\s*=\s*([^|]+)\s*\|/' , $ac , $m ) ) setStringProp ( 'P651' , $m[1] ) ;
	if ( preg_match ( '/\|\s*BIBSYS\s*=\s*([^|]+)\s*\|/' , $ac , $m ) ) setStringProp ( 'P1015' , $m[1] ) ;
	if ( preg_match ( '/\|\s*ULAN\s*=\s*([^|]+)\s*\|/' , $ac , $m ) ) setStringProp ( 'P245' , $m[1] ) ;
	if ( preg_match ( '/\|\s*MBA\s*=\s*([^|]+)\s*\|/' , $ac , $m ) ) setStringProp ( 'P434' , $m[1] ) ;

/*	if ( preg_match ( '/\|\s*TYP\s*=\s*([^|]+)\s*\|/' , $ac , $m ) ) {
		if ( trim($m[1]) == 'p' ) setItemProp ( 'P31' , 'Q5' ) ;
	}*/
}

function useBestDate ( $p ) {
	global $gsdata ;
	$v = $gsdata['out'][$p] ;
	$best_date = '' ;
	$best_score = -1 ;
	foreach ( $v AS $num => $d ) {
		$x = explode ( '/' , $d[0] ) ;
		if ( $x[1]*1 <= $best_score ) continue ;
		$best_score = $x[1]*1 ;
		$best_date = $d ;
	}
	$gsdata['out'][$p] = array ( $best_date ) ;
}

function loadWikitext ( $lang , $page ) {
	$w = file_get_contents ( 'http://' . $lang . '.wikipedia.org/w/index.php?title='.urlencode($page)."&action=raw" ) ;
	$w = preg_replace ( '/<!--[\s\S]*?-->/' , '' , $w ) ; // Remove HTML comments
	return $w ;
}


function myArrayUnique ( $a ) {
	if ( count ( $a ) < 2 ) return $a ;
	$b = array() ;
	foreach ( $a AS $dummy => $v ) {
		$k = array_shift ( $v ) ;
		while ( count ( $v ) > 0 ) {
			$k1 = array_shift ( $v ) ;
			$v1 = array_shift ( $v ) ;
			$b[$k][$k1][] = $v1 ;
			$b[$k][$k1] = array_unique ( $b[$k][$k1] ) ;
		}
	}
	$ret = array() ;
	foreach ( $b AS $k0 => $v0 ) {
		$n = array ( $k0 ) ;
		foreach ( $v0 AS $k1 => $v1 ) {
			foreach ( $v1 AS $k2 => $v2 ) {
				$n[] = $k1 ;
				$n[] = $v2 ;
			}
		}
		$ret[] = $n ;
	}
	return $ret ;
}

function addAllLinks ( $prop , $s , $wiki) {
	global $gsdata ;
	$s = preg_replace ( '/<br\s*\/{0,1}\s*>/i' , ' ' , $s ) ;
	$s = preg_replace ( '/<.+?<\/.+?>/' , '' , $s ) ;
	$s .= ' [[test|bla]]' ;
	preg_match_all ( '/\[\[(.+?)(\|.*){0,1}\]\]/' , $s , $m ) ;
	if ( count ( $m ) < 2 ) return ;

	$db = $gsdata['db'] ;
	foreach ( $m[1] AS $k => $v ) {
		$v = $db->real_escape_string ( $v ) ;
		$sql = "SELECT ips_item_id FROM wb_items_per_site WHERE ips_site_id='$wiki' and ips_site_page='$v'" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $gsdata['db']->error . ']: '.$sql);
		while($o = $result->fetch_object()){
//			print $v . " : " . $o->ips_item_id . "\n" ;
			setItemProp ( $prop , 'Q'.$o->ips_item_id ) ;
		}
	}
}

//____________________ MAIN


function run_gs ( $q ) {
	global $gsdata ;

	if ( !isset($gsdata['months']) ) {
		$months_tmp = json_decode ( file_get_contents ( 'months.json' ) ) ; // Generated by THE GAME
		$gsdata['months'] = array () ;
		foreach ( $months_tmp AS $lang => $v ) {
			foreach ( $v AS $name => $num ) $gsdata['months'][$name] = $num ;
		}
	}

	if ( !isset($gsdata['db']) ) {
		$gsdata['db'] = openDB ( 'wikidata' , '' ) ;
	}

	$q = preg_replace ( '/\D/' , '' , $q ) ; // No leading "Q"!
	$gsdata['q'] = $q ;

	$sql = "select * from wb_items_per_site where ips_item_id=$q" ;
	if(!$result = $gsdata['db']->query($sql)) die('There was an error running the query [' . $gsdata['db']->error . ']: '.$sql);
	$sites = array() ;
	while($o = $result->fetch_object()){
		$sites[$o->ips_site_id] = str_replace ( ' ' , '_' , $o->ips_site_page ) ;
	}

	$sql = "select distinct pl_title from wb_entity_per_page,pagelinks where epp_entity_id=$q and epp_entity_type='item' and pl_from=epp_page_id and pl_namespace=120" ;
	if(!$result = $gsdata['db']->query($sql)) die('There was an error running the query [' . $gsdata['db']->error . ']: '.$sql);
	$existing_props = array() ;
	while($o = $result->fetch_object()){
		$existing_props[$o->pl_title] = true ;
	}

	// BEGIN PARSING

	$gsdata['out'] = array() ;

	$gsdata['imported_from'] = '' ;

	if ( isset ( $sites['dewiki'] ) ) {
		$gsdata['imported_from'] = 'Q48183' ;
		$page = $sites['dewiki'] ;
		$w = loadWikitext ( 'de' , $page ) ;
		$w = preg_replace ( '/\bv. Chr.\b/' , 'BC' , $w ) ; // FIXME
		$pd = getTemplate ( 'Personendaten' , $w ) ;
	//	if ( $pd != '' ) setItemProp ( 'P31' , 'Q5' ) ;
		if ( preg_match ( '/\|\s*GEBURTSDATUM\s*=\s*([^|]+)\s*\|/' , $pd , $m ) ) setDateProp ( 'P569' , $m[1] ) ;
		if ( preg_match ( '/\|\s*STERBEDATUM\s*=\s*([^|]+)\s*\|/' , $pd , $m ) ) setDateProp ( 'P570' , $m[1] ) ;
		$nd = getTemplate ( 'Normdaten' , $w ) ;
		checkAC ( $nd ) ;
	}

	if ( isset ( $sites['enwiki'] ) ) {
		$gsdata['imported_from'] = 'Q328' ;
		$page = $sites['enwiki'] ;
		$w = loadWikitext ( 'en' , $page ) ;
		$w = preg_replace ( '/\|\s*df\s*=\s*[a-z]*\|/' , '|' , $w ) ; // Date template pain
	//	$w = preg_replace ( '/\{\{birth[ -_]year\s*\|\s*(\d+)\.*?\}\}/i' , '$1' , $w ) ; // {{Birth year|1599
	//	$w = preg_replace ( '/\{\{birth[ -_]year[ _]and[ _]age\s*\|\s*(\d+)\s*\|\.*?\}\}/i' , '$1' , $w ) ; // {{Birth year|1599
	//	$w = preg_replace ( '/\{\{death[ -_]year\s*\|\s*(\d+)\.*?\}\}/i' , '$1' , $w ) ; // {{Birth year|1599
	//	$w = preg_replace ( '/\{\{death[ -_]year[ _]and[ _]age\s*\|\s*(\d+)\s*\|\.*?\}\}/i' , '$1' , $w ) ; // {{Birth year|1599

		$w = preg_replace ( '/\{\{birth[ -_]date\s*\|\s*(\d+)\s*\|\s*(\d+)\s*\|\s*(\d+).*?\}\}/i' , '$1-$2-$3' , $w ) ; // {{Birth date|1599|6|6
		$w = preg_replace ( '/\{\{death[ -_]date\s*\|\s*(\d+)\s*\|\s*(\d+)\s*\|\s*(\d+).*?\}\}/i' , '$1-$2-$3' , $w ) ; // {{Death date|1599|6|6
		$w = preg_replace ( '/\{\{birth[ -_]date[ _]and[ _]age\s*\|\s*(\d+)\s*\|\s*(\d+)\s*\|\s*(\d+).*?\}\}/i' , '$1-$2-$3' , $w ) ; // {{Birth date and age|1599|6|6
		$w = preg_replace ( '/\{\{death[ -_]date[ _]and[ _]age\s*\|\s*(\d+)\s*\|\s*(\d+)\s*\|\s*(\d+).*?\}\}/i' , '$1-$2-$3' , $w ) ; // {{Death date and age|1599|6|6
		$w = preg_replace ( '/\{\{cite\b.+?\}\}/' , '' , $w ) ;
		$pd = getTemplate ( 'Persondata' , $w ) ;
	//	if ( $pd != '' ) setItemProp ( 'P31' , 'Q5' ) ;
		if ( preg_match ( '/\|\s*DATE OF BIRTH\s*=\s*([^|]+)\s*\|/' , $pd , $m ) ) setDateProp ( 'P569' , $m[1] ) ;
		if ( preg_match ( '/\|\s*DATE OF DEATH\s*=\s*([^|]+)\s*\|/' , $pd , $m ) ) setDateProp ( 'P570' , $m[1] ) ;
		$ib = getTemplate ( 'Infobox' , $w ) ;
		if ( preg_match ( '/\|\s*birth_date\s*=\s*([^|]+)\s*\|/' , $ib , $m ) ) setDateProp ( 'P569' , $m[1] ) ;
		if ( preg_match ( '/\|\s*death_date\s*=\s*([^|]+)\s*\|/' , $ib , $m ) ) setDateProp ( 'P570' , $m[1] ) ;
		if ( preg_match ( '/\|\s*alma_mater\s*=\s*([^|]+)\s*\|/' , $ib , $m ) ) addAllLinks ( 'P69' , $m[1] , 'enwiki' ) ;
		if ( preg_match ( '/\|\s*occupation\s*=\s*([^|]+)\s*\|/' , $ib , $m ) ) addAllLinks ( 'P106' , $m[1] , 'enwiki' ) ;
		if ( preg_match ( '/\|\s*image\s*=\s*([^|]+\..{3,4})\s*\|/' , $ib , $m ) ) {
			$file = $m[1] ;
			$file = preg_replace ( '/^\[\[/' , '' , $file ) ;
			$file = preg_replace ( '/^(File|Image):/i' , '' , $file ) ;
	//		if ( !preg_match ( '/\bremoved\b/i' , $file ) ) setStringProp ( 'P18' , $file ) ; // FIXME Ensure it's a Commons file...
		}
		$ow = getTemplate ( 'official website' , $w ) ;
		if ( preg_match ( '/^\s*\|{0,1}([^|]+)\|/' , $ow , $m ) ) {
			$url = $m[1] ;
			if ( !preg_match ( '/^https{0,1}:\/\//i' , $url ) ) $url = "http://$url" ;
			setStringProp ( 'P856' , $url ) ;
		}
		$misc = getTemplate ( 'twitter' , $w ) ;
		if ( preg_match ( '/^\s*\|{0,1}([^|]+)\|/' , $misc , $m ) ) setStringQualProp ( 'P553' , 'Q918' , 'P554' , $m[1] ) ;
		$misc = getTemplate ( 'IMDb name' , $w ) ;
		if ( $misc!='' and preg_match ( '/\|\s*id\s*=\s*([^|]+)\s*\|/' , $misc , $m ) ) setStringProp ( 'P345' , 'nm'.padStringWithZeros($m[1],7) ) ;
		if ( preg_match ( '/\{\{[Cc]ommons category\}\}/' , $w ) ) setStringProp ( 'P373' , str_replace('_',' ',$page) ) ;

		$ac = getTemplate ( 'Authority control' , $w ) ;
		checkAC ( $ac ) ;
	}

	// Cleanup output
	foreach ( $gsdata['out'] AS $p => $v ) {
		if ( isset ( $existing_props[$p] ) ) {
			unset ( $gsdata['out'][$p] ) ;
		} else {
			$gsdata['out'][$p] = myArrayUnique ( $v ) ;
			if ( ($p=='P569' or $p=='P570') and count($gsdata['out'][$p])>1 ) useBestDate($p) ;
		}
	}

	$ret = array() ;
	foreach ( $gsdata['out'] AS $p => $v ) {
		foreach ( $v AS $dummy => $value ) {
			if ( is_array ( $value ) ) $value = join ( "\t" , $value ) ;
			$value = trim ( $value ) ;
			if ( $value == '' ) continue ;
			$s = "Q$q\t$p\t$value" ;
			$ret[] = $s ;
		}
	}
	
	return $ret ;
}


?>