<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR); // 
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

require_once ( 'php/common.php' ) ;

print get_common_header ( '' , 'Duplicity' ) ;

$wiki = get_request ( 'wiki' , '' ) ;
$mode = get_request ( 'mode' , '' ) ;
$testing = isset($_REQUEST['testing']) ;

$dbu = openToolDB ( 'duplicity_p' ) ;
$dbw = openDB ( 'wikidata' , 'wikidata' ) ;

if ( $mode == 'stats' and $wiki != '' ) {

	$wiki = $dbu->real_escape_string ( $wiki ) ;
	print "<h2>Number of articles without Wikidata item on <a href='?wiki=$wiki&mode=list'>$wiki</a></h2>" ;

	$d = array() ;
	$wikis = array() ;
	$dates = array() ;
	$sql = "select * FROM `stats` where wiki='$wiki'" ;
	$result = getSQL ( $dbu , $sql ) ;
	while($o = $result->fetch_object()) {
		$d[$o->wiki][$o->date] = $o->count ;
		$wikis[''.$o->wiki] = 1 ;
		$dates[''.$o->date] =$o->date ;
	}
	ksort ( $d ) ;
	ksort ( $dates ) ;
	ksort ( $wikis ) ;
	
	
	print "<script>\n" ;

	print "var data = [ " ;
	$first = true ;
	foreach ( $d AS $wiki => $d0 ) {
		if ( $first ) $first = false ;
		else print "," ;
		print "[" ;
		$ffirst = true ;
		foreach ( $d0 AS $x => $cnt ) {
			if ( $ffirst ) $ffirst = false ;
			else print "," ;
			$day = substr($x,0,4).'-'.substr($x,4,2).'-'.substr($x,6,2).'Z00:00:00T' ;
			$xcoord = strtotime($day) ;
			print "[$xcoord,$cnt]" ;
		}
		print "]" ;
	}
	print "];\n" ;
	
	$every_nth_day = 1 ;
	while ( count($dates) / $every_nth_day > 15 ) $every_nth_day++ ;
	print "var xticks = [" ;
	$last = '' ;
	$cnt = 0 ;
	$first = true ;
	foreach ( $dates AS $x ) {
		$day = substr($x,0,4).'-'.substr($x,4,2).'-'.substr($x,6,2).'Z00:00:00T' ;
		if ( $day == $last ) continue ;
		$last = $day ;
		$cnt++ ;
		if ( $cnt % $every_nth_day != 0 ) continue ;
		$xcoord = strtotime($day) ;
		$day = substr ( $day , 5 , 5 ) ;
		if ( $first ) $first = false ;
		else print "," ;
		print "[$xcoord,'$day']" ;
	}
	print "];\n" ;
	

?>

</script>


<script src="resources/js/flot/jquery.flot.min.js"></script>


<div id='graph' style='width:940px;height:500px;'>
</div>

<script>

$(document).ready ( function () {
	$.plot("#graph", data , {
		series: {
				lines: { show: true },
				points: { show: true }
			},
		xaxis : {
			ticks : xticks
		} , 
		yaxis : {
			min:0
		} ,
		grid: {
			backgroundColor: { colors: [ "#fff", "#eee" ] }
		}
	});
} ) ;
</script>

</body>
</html>

<?PHP
exit ( 0 ) ;
}



print "<script src='/magnustools/resources/js/jquery/stupidtable.js'></script>" ;
print '<script>$(document).ready(function(){$("table").stupidtable();});</script>' ;


if ( $mode == 'cats' and $wiki != '' ) {

	$min_pages = 10 ;
	print "<div class='lead'>These are the most-used categories for articles without Wikidata item on $wiki (minimum of $min_pages pages):</div>" ;
	print "<div><table class='table table-striped'>" ;
	print "<thead><tr><th>Category</th><th># articles</th></tr></thead><tbody>" ;

	$pages = array() ;
	$sql = "SELECT * FROM no_wd WHERE wiki='" . $dbu->real_escape_string ( $wiki ) . "'" ;
	$result = getSQL ( $dbu , $sql ) ;
	while($o = $result->fetch_object()) {
		$pages[] = $dbu->real_escape_string ( $o->title ) ;
	}

	$dbw = openDBwiki ( $wiki ) ;
	$sql = 'select cl_to,count(*) AS cnt from page,categorylinks where page_id=cl_from and page_title in ("' . implode ( '","' , $pages ) . '") and page_namespace=0 and page_is_redirect=0 group by cl_to HAVING cnt >= ' . $min_pages . ' order by cnt desc' ;
	$result = getSQL ( $dbw , $sql ) ;
	while($o = $result->fetch_object()) {
		print "<tr>" ;
		print "<td>" . str_replace ( '_' , ' ' , $o->cl_to ) . "</td>" ;
		print "<td style='text-align:right;font-family:courier'>" . number_format ( $o->cnt ) . "</td>" ;
		print "</tr>" ;
	}
	
	print "</tbody></table></div>" ;
	print get_common_footer() ;
	exit ( 0 ) ;
}


if ( $mode == 'list' and $wiki != '' ) {

	$cat = trim ( get_request ( 'cat' , '' ) ) ;
	$dbwp = openDBwiki ( $wiki ) ;
	
	$server = getWebserverForWiki ( $wiki ) ;
	print "<div class='lead'>These are articles:<ul><li>on $wiki (see <a href='?mode=stats&wiki=$wiki'>stats</a>; see <a href='?mode=cats&wiki=$wiki'>popular categories</a>)</li><li>without Wikidata item</li><li>created >2 weeks ago</li><li>excluding certain maintenance categories</li><li>ordered by oldest articles first</li>" ;
	if ( $cat != '' ) print "<li>in category <tt>$cat</tt> or any of its sub-categories" ;
	print "</ul></div>" ;
	print "<div><form class='form form-inline' method='get'>" ;
	print "<input type='text' name='cat' value='" . escape_attribute($cat) . "' />" ;
	print "<input type='hidden' name='mode' value='list' />" ;
	print "<input type='hidden' name='wiki' value='" . escape_attribute($wiki) . "' />" ;
	print " <input type='submit' value='Filter by (sub)categories' class='btn btn-primary' />" ;
	print "</form></div>" ;
	print "<div><table class='table table-striped'>" ;
	print "<thead><tr><th style='text-align:right'>#</th><th>Article</th><th>Actions</th><th nowrap>Article created</th></tr></thead><tbody>" ;
	$candidates = [] ;
	$in_cat = [] ;
	$already_done = [] ;
#	$pt = [] ;

	if ( 0 ) { # Use PetScan; does not work yet
		$server = getWebserverForWiki($wiki) ;
		preg_match ( '/^(.+?)\.(.+?)\./' , $server , $m ) ;
		$language = $m[1] ;
		$project = $m[2] ;
		$url = "http://petscan.wmflabs.org/?language={$language}&project={$project}&depth=10&categories=" ;
			urlencode($cat) .
			"&ns[0]=1&show_redirects=no&wikidata_item=without&doit=&format=json&output_compatability=quick-intersection&sparse=1" ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		foreach ( $j->pages AS $p ) $in_cat[str_replace('_',' ',$p)] = 1 ;
	} else {
		
		if ( $cat != '' ) {
			$subcats = array() ;
			findSubcats ( $dbwp , array($cat) , $subcats , -1 ) ;
			foreach ( $subcats AS $k => $v ) $subcats[$k] = $dbwp->real_escape_string ( $v ) ;
			if ( count($subcats)>0 ) { #and count($pt) > 0 ) {
				$sql = "SELECT DISTINCT page_title FROM page,categorylinks WHERE page_id=cl_from and page_namespace=0 and page_is_redirect=0 " ;
				$sql .= " AND cl_to IN ('" . implode ( "','" , $subcats ) . "')" ;
				$sql .= " AND NOT EXISTS (SELECT * FROM page_props WHERE pp_page=page_id AND pp_propname='wikibase_item')" ;
#				$sql .= " AND page_title IN ('" . implode ( "','" , $pt ) . "')" ;
#				unset ( $pt ) ;
				unset ( $subcats ) ;
				$result = getSQL ( $dbwp , $sql ) ;
				while($o = $result->fetch_object()) {
					$in_cat[str_replace('_',' ',$o->page_title)] = 1 ;
				}
			}
		}

		$sql = "SELECT * FROM no_wd WHERE wiki='" . $dbu->real_escape_string ( $wiki ) . "' ORDER BY creation_date ASC" ; //  AND tmp_flag=0
		$result = getSQL ( $dbu , $sql ) ;
		while($o = $result->fetch_object()) {
			if ( $cat != '' and !isset($in_cat[$o->title]) ) continue ;
			$candidates[] = $o ;
#			if ( $cat != '' ) $pt[] = $dbwp->real_escape_string ( str_replace(' ','_',$o->title) ) ;
		}

		if ( count($candidates) < 10000 ) { # Just making up a number
			$sql = [] ;
			foreach ( $candidates AS $o ) {
				if ( $cat != '' and !isset($in_cat[$o->title]) ) continue ;
				$sql[] = '"' . $dbu->real_escape_string ( str_replace(' ','_',$o->title) ) . '"' ;
			}
			if ( count($sql) > 0 ) {
				$sql = "SELECT page_title FROM page WHERE (page_is_redirect=1 OR EXISTS (SELECT * FROM page_props WHERE pp_page=page_id AND pp_propname='wikibase_item') ) AND page_namespace=0 AND page_title IN (" . implode(',',$sql) . ")" ;
				$result = getSQL ( $dbwp , $sql ) ;
				while($o = $result->fetch_object()) {
					$already_done[str_replace('_',' ',$o->page_title)] = 1 ;
				}
			}
		}


	}

	$cnt = 0 ;
	foreach ( $candidates AS $o ) {
		if ( isset($already_done[$o->title]) ) continue ;
		if ( $cat != '' and !isset($in_cat[$o->title]) ) continue ;
		$cnt++ ;
		print "<tr>" ;
		print "<td nowrap style='text-align:right;font-family:Courier;'>" . number_format ( $cnt ) . "</td>" ;
		print "<th><a href='https://$server/wiki/" . myurlencode($o->title) . "' target='_blank'>" . $o->title . "</a></th>" ;
		
		print "<td nowrap><a href='?wiki=$wiki&norand=1&page=" . urlencode($o->title) . "' target='_blank'>check</a></td>" ;
		
		$d = date_parse ( $o->creation_date ) ;
		print "<td nowrap>" ;
		print $d['year'] . '-' . str_pad($d['month'],2,'0',STR_PAD_LEFT) . '-' . str_pad($d['day'],2,'0',STR_PAD_LEFT) . ' ' . str_pad($d['hour'],2,'0',STR_PAD_LEFT) . ':' . str_pad($d['minute'],2,'0',STR_PAD_LEFT) . ':' . str_pad($d['second'],2,'0',STR_PAD_LEFT) ;
		print "</td>" ;
		
		print "</tr>\n" ;
	}
	print "</tbody></table></div>" ;
	print get_common_footer() ;
	myflush();
	exit ( 0 ) ;
}







if ( $wiki == '' ) {
?>


<div class='lead'>
This tool can pick a random article on a wiki without associated Wikidata item, and offer some possible matches on Wikidata, so you can add it to an existing item, or create a new one.<br/>
</div>

<p>Articles must have been created over two weeks ago, and not contain a "bad" category (e.g. deletion request).</p>

<?PHP

	print "<h2>Available wikis</h2>" ;
	print "<table class='table table-striped'>" ;
	print "<thead><tr><th data-sort='string'>Wiki</th><th>List</th><th>Statistics</th><th>Random</th><th style='text-align:right' data-sort='int'>Articles without item</th></tr></thead>" ;
	print "<tbody>" ;
	$sql = "select wiki,count(*) AS cnt from no_wd group by wiki" ;
	$result = getSQL ( $dbu , $sql ) ;
	while($o = $result->fetch_object()) {
		print "<tr>" ;
		print "<th>{$o->wiki}</th>" ;
		print "<td><a href='?wiki={$o->wiki}&mode=list'>show list</a></td>" ;
		print "<td><a href='?wiki={$o->wiki}&mode=stats'>show stats</a></td>" ;
		print "<td><a href='?wiki={$o->wiki}'>check random article</a></td>" ;
		print "<td style='text-align:right;font-family:courier' data-sort-value='{$o->cnt}'>" . number_format($o->cnt) . "</td>" ;
		print "</tr>" ;
//		print "<li><a href='?wiki={$o->wiki}'>{$o->wiki}</a> ({$o->cnt} articles without item)</li>" ;
	}
	print "</tbody></table>" ;

//	print "<form method='get' class='form form-inline'>Wiki <input type='text' name='wiki' placeholder='e.g. enwiki, dewiki' /> <input type='submit' class='btn btn-primary' value='Get a random article without Wikidata item' /></form>" ;
	print get_common_footer() ;
	exit ( 0 ) ;
}


$boxh = '1000px' ;

$wiki = $dbu->real_escape_string ( $wiki ) ;
$server = getWebserverForWiki ( $wiki ) ;
$server = preg_replace ( '/^([^.]+)\./' , "$1.m." , $server ) ;
$force_page = $dbu->real_escape_string ( get_request ( 'page' , '' ) ) ;
$specific_page = $force_page != '' ;
$norand = isset($_REQUEST['norand']) ;

$title = '' ;
while ( 1 ) {
	$title = '' ;
	$id = '' ;
	

	if ( $specific_page ) {
		$sql = "SELECT * FROM no_wd WHERE wiki='$wiki' AND title='$force_page'" ;
		$result = getSQL ( $dbu , $sql ) ;
		while($o = $result->fetch_object()){
			$title = $o->title ;
			$id = $o->id ;
		}
	}

	if ( $norand and $specific_page and $title == '' ) { // Not in duplicity database, but treat as if
		$title = get_request ( 'page' , '' ) ;
		$id = -1 ;
	} else {
		while ( $title == '' ) {
			$r = mt_rand()/mt_getrandmax() ;
			$sql = "SELECT * FROM no_wd WHERE wiki='$wiki' AND random>=$r ORDER BY random LIMIT 1" ;
			$result = getSQL ( $dbu , $sql ) ;
			while($o = $result->fetch_object()){
				$title = $o->title ;
				$id = $o->id ;
			}
		}
	}

	$found = false ;
	$sql = "SELECT * FROM wb_items_per_site WHERE ips_site_id='$wiki' AND ips_site_page='" . $dbw->real_escape_string($title) . "'" ;
	$result = getSQL ( $dbw , $sql ) ;
	while($o = $result->fetch_object()) $found = true ;
	if ( $found ) { // Has an item assigned, remove from cache
		$sql = "DELETE FROM no_wd WHERE id=$id" ;
		$result = getSQL ( $dbu , $sql ) ;
		if ( $norand ) break ;
		continue ;
	} else {
		break ;
	}
	
	if ( $norand ) break ;
}

$url = "https://$server/wiki/".myurlencode($title) ;
?>

<form id='button_row' class='form form-inline inline-form' style='margin-left:300px'>
	<div style='display:inline-block;margin-left:50px'><button class='btn btn-outline-danger' id='do_skip'>Skip</button></div>
	<div style='display:inline-block;margin-left:20px'><button class='btn btn-outline-info' id='do_create'>Create new item</button></div>
	<div style='display:inline-block;margin-left:20px'><button class='btn btn-outline-info' id='do_create_disambig'>Create new disambiguation item</button></div>
	<div style='display:inline-block;margin-left:20px'><button class='btn btn-outline-info' id='do_create_list'>Create new list item</button></div>
	<div style='display:inline-block;margin-left:20px'><button class='btn btn-outline-info' id='do_create_human'>Create new human</button></div>
	<div style='margin-left:20px;display:inline-block'><button class='btn btn-outline-success' id='do_add'></button></div>
	<div style='margin-left:20px;display:inline-block' id='widar_note'></div>
</form>

</div>
</div>
<div class='row'>

<?PHP

// WP preview
print "<div class='col-sm'><iframe style='margin-right:5px;width:100%;display:inline-block;height:$boxh' src='$url'></iframe></div>" ;

$t2 = str_replace ( '_' , ' ' , $title ) ;
$t2 = preg_replace ( '/ von /' , ' ' , $t2 ) ;
$t2 = preg_replace ( '/-/' , ' ' , $t2 ) ;
$t2 = preg_replace ( '/[A-Z]\./' , ' ' , $t2 ) ;
$t2 = preg_replace ( '/^[a-z ]+:/i' , '' , $t2 ) ;
$t2 = escape_attribute ( trim ( preg_replace ( '/\s*\(.+?\)\s*/' , ' ' , $t2 ) ) ) ;

// Search column
print "<div class='col-sm-2'>" ;
$h = '' ;
//$h .= '</div><div style="">' ;
$h .= "<div style='display:inline-block;height:$boxh;max-height:$boxh;overflow:auto'>" ; // width:{$perc[1]}%;
$h .= "<div style='margin-bottom:10px'>" ;
$h .= "<form id='search_form' class='form form-inline inline-form'>" ;
$h .= "<div class='input-group'>" ;
$h .= "<input class='input-element' style='width:auto' name='query' type='text' value='$t2' />" ;
$h .= "<span class='input-group-btn'><input type='submit' class='btn btn-secondary' value='&#128269;'></span>" ;
$h .= "</div>" ;
$h .= "</form>" ;
$h .= "</div>" ;
$h .= "<ol id='search_results'>" ;
$h .= "</ol></div>" ;
$h .= "</div>" ;
print $h ;

// WD preview
print "<div class='col-sm'><iframe id='wd_preview' style='margin-left:5px;display:inline-block;width:100%;height:$boxh' src=''></iframe></div>" ;


?>
</div>

<style>
#working {
	display:inline;
	margin-top:5px;
	margin-left:30px;
	padding:2px;
	color:#FF4848;
}
li.selected {
	background-color:#DDD;
}
li.has_sitelink {
	border-right:3px solid #FF4848 !important;
}
</style>

<!--<script src="//en.wikipedia.org/w/index.php?title=MediaWiki:Wdsearch-autodesc.js&action=raw&ctype=text/javascript"></script>-->
<script src="//tools-static.wmflabs.org/magnustools/resources/js/wikidata.js"></script>

<script>

var wd = new WikiData() ;

var widar_api = '/widar/index.php?' ;
var wiki = '<?PHP print addslashes($wiki) ?>' ;
var title = '<?PHP print addslashes($title) ?>' ;
var testing = '<?PHP print ($testing?1:0) ?>' ;
var specific_page = '<?PHP print ($specific_page?1:0) ?>' ;
var widar_logged_in = false ;
var work_cnt = 0 ;

function working ( x ) {
	work_cnt += x ? 1 : -1 ;
	if ( work_cnt > 0 ) {
		$('#button_row button').prop('disabled', true);
		$('#working').show();//.css({display:'inline-block',height:'0px'}) ;
	} else {
		$('#button_row button').prop('disabled', false);
		$('#working').hide() ;
	}
}

function showWD ( q ) {
	$('#wd_preview').attr ( { src:'about:blank' } ) ;
	setTimeout ( function () {
		var url = "https://www.wikidata.org/wiki/" + q ;
		$('#wd_preview').attr ( { src:url } ) ;
		$('li.search_result').removeClass ( 'selected' ) ;
		$('li.search_result[q="'+q+'"]').addClass ( 'selected' ) ;
		$('#do_add').show().text("Add to "+q).attr({q:q}) ;
	} , 10 ) ;
}

function descMarkup ( s ) {
//	if ( null != s.match ( /^Wikimedia/ ) ) s = '<span style="#89FC63">' + d.manual_description + '</span>' ;
	s = s.replace ( /^(Wiki[mp]edia .+)$/i , '<span style="background-color:#89FC63">$1</span>' ) ;
	return s ;
}

function runAutodesc () {
	$('div.autodesc').each ( function () {
		var o = $(this) ;
		var q = o.attr('q') ;
		$.getJSON ( '/autodesc/?q='+q+'&lang=en&mode=short&links=text&redlinks=&format=json&get_infobox=yes&callback=?' , function ( d ) {
			var t = [] ;
			if ( d.manual_description != '' ) t.push ( descMarkup ( d.manual_description ) ) ;
			if ( d.result != '' ) t.push ( descMarkup ( d.result ) ) ;
			t = t.join ( '<br/>' ) ;
			o.html ( t ) ;
			if ( d.label != q ) {
				$('a.search_result[q="'+q+'"]').html ( d.label + " <small>[" + q + "]</small>" ) ;
			}
		} ) ;
	} ) ;

	$('a.search_result').click ( function () {
		var o = $(this) ;
		var q = o.attr('q') ;
		showWD ( q ) ;
		return false ;
	} ) ;
}

function doSearch () {
	$('#wd_preview').attr ( { src:'about:blank' } ) ;
	$('#search_results').html ( '' ) ;
	$('#do_add').hide() ;
	var query = $('input[name="query"]').val() ;
	var url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=500&format=json&srsearch=" + encodeURIComponent ( query ) + "&callback=?" ;
	working(true) ;
	$.getJSON ( url , function ( d ) {
		var to_load = [] ;
		$.each ( d.query.search , function ( k , v ) { to_load.push ( v.title ) } ) ;
		wd.getItemBatch ( to_load , function () {
			working(false) ;
			var h = '' ;
			var first_q ;
			$.each ( d.query.search , function ( k , v ) {
				if ( typeof first_q == 'undefined' ) first_q = v.title ;
				var i = wd.getItem ( v.title ) ;
				var sitelinks = i.getWikiLinks() ;
				var has_sitelink = typeof sitelinks[wiki] != 'undefined' ;
				
				h += "<li q='" + v.title + "' class='search_result" ;
				if ( has_sitelink ) h += " has_sitelink" ;
				h += "'" ;
				if ( has_sitelink ) h += " title='This item already has a site link to "+wiki+"'" ;
				h += ">" ;
				h += "<a class='search_result' q='" + v.title + "' href='#'>" + v.title + "</a>" ;
				h += "<div style='font-size:8pt' class='autodesc' q='" + v.title + "'></div>" ;
				h += "</li>" ;
			
			} ) ;
			if ( d.query.search.length == 0 ) h += "<li><i>No results for search:</i><br/>" + query + "</li>" ;
			$('#search_results').html ( h ) ;
			runAutodesc() ;
			if ( typeof first_q != 'undefined' ) showWD ( first_q ) ;
		} ) ;
	} ) ;
}

function checkOauthStatus () {
	$.get ( widar_api , {
		action:'get_rights',
		botmode:1
	} , function ( d ) {
		var h = '' ;
		if ( d.error != 'OK' || typeof (d.result||{}).error != 'undefined' ) {
			h = "You have not authorized <a target='_blank' href='/widar/index.php?action=authorize'>WiDaR</a>, you'll need to add site links through a form." ;
		} else {
			h = "Welcome, <a target='_blank' href='//www.wikidata.org/wiki/User:" + encodeURIComponent(d.result.query.userinfo.name) + "'>" + d.result.query.userinfo.name + "</a>!" ;
			widar_logged_in = true ;
			$('#do_create_disambig').show() ;
			$('#do_create_list').show() ;
			$('#do_create_human').show() ;
		}
		$('#widar_note').html ( h ) ;
	} , 'json' ) ;
}


function onClickAdd () {
	var q = $('#do_add').attr('q') ;

	if ( widar_logged_in ) {
		working(true) ;
	
		var o = {
			action : 'set_sitelink' ,
			botmode : 1 ,
			q : q ,
			site : wiki ,
			title : title
		} ;

		$.get ( widar_api , o , function ( d ) {
			working(false) ;
			if ( d.error == 'OK' ) {
				loadNextItem() ;
			} else {
				alert ( d.error ) ;
				console.log ( d ) ;
			}
		} , 'json' ) ;

	} else {

		var tu = encodeURIComponent(title.replace(/_/g,' ')) ;
		var url = "https://www.wikidata.org/wiki/Special:SetSiteLink?id=" + q + "&site=" + wiki + "&page=" + tu ;
		var win = window.open(url, '_blank');
		loadNextItem() ;

	}
	
	return false ;
}

function loadNextItem () {
	working(true) ;
	if ( specific_page*1 ) { window.close() ; return }
	window.location.href = 'https://tools.wmflabs.org/wikidata-todo/duplicity.php?wiki='+wiki ;
}

function newItemWithInstance ( inst ) {
	if ( !widar_logged_in ) return false ;

	working(true) ;
	$.get ( widar_api , {
		action : 'create_item_from_page' ,
		botmode : 1 ,
		site : wiki ,
		page : title
	} , function ( d ) {
		if ( d.error == 'OK' ) {
			
			console.log ( "Created " + d.q ) ;

			$.getJSON ( widar_api , {
				action:'set_claims',
				ids:'Q'+(d.q+'').replace(/\D/g,''),
				prop:'P31',
				target:inst,
				botmode:1
			} , function ( d2 ) {
				working(false) ;
				if ( d2.error == 'OK' ) {
					loadNextItem() ;
				} else {
					console.log ( d2 ) ;
					alert ( d2.error ) ;
				}
			} ) ;
			
			
		} else {
			alert ( d.error ) ;
			console.log ( d ) ;
			working(false) ;
		}
	} , 'json' ) ;


	return false ;
}

$(document).ready ( function () {

	$('#do_create_disambig').hide() ;
	$('#do_create_list').hide() ;
	$('#do_create_human').hide() ;
	
	$('#toolname').after ( "<div id='working' class='nav'>Working...</div>" ) ;
	$('#working').hide() ;

	checkOauthStatus() ;
	$('#main_content').removeClass('container').addClass('container-fluid') ;
//	$('#main_content > div.row').removeClass('row').addClass('row-fluid') ;

	$('#do_skip').click ( function () {
		loadNextItem() ;
		return false ;
	} ) ;
	
	
	
	$('#do_create_disambig').click ( function () {
		return newItemWithInstance ( 'Q4167410' ) ;
	} ) ;
	
	$('#do_create_list').click ( function () {
		return newItemWithInstance ( 'Q13406463' ) ;
	} ) ;
	
	$('#do_create_human').click ( function () {
		return newItemWithInstance ( 'Q5' ) ;
	} ) ;
	
	
	
	
	$('#do_create').click ( function () {
		var tu = encodeURIComponent(title.replace(/_/g,' ').replace(/^[a-z ]+:/i,'')) ;
		var lang_code = wiki.replace(/wiki.*$/,'') ;
		if ( lang_code == 'species' || lang_code == 'commons' ) lang_code = 'en' ;
		if ( lang_code == 'no' ) lang_code = 'nb' ;
		var url = "https://www.wikidata.org/wiki/Special:NewItem?lang=" + lang_code + "&site=" + wiki + "&page=" + tu + "&label=" + $.trim ( tu.replace ( /\s*\(.+?\)\s*/ , ' ' ) ) ;
		var win = window.open(url, '_blank');
		loadNextItem() ;
		return false ;
	} ) ;
	
	$('#do_add').click ( onClickAdd ) ;

	$('#search_form').submit ( function ( e ) {
		doSearch() ;
		return false ;
	} ) ;

	doSearch () ;


} ) ;
</script>



<?PHP


print get_common_footer() ;

?>