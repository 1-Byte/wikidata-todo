<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;

$format = get_request ( 'format' , 'dot' ) ;

print get_common_header ( '' , 'WDQ-to-Graph' ) ;
print "<p class='lead'>Given a root item and one or more properties, generate a <a href='https://en.wikipedia.org/wiki/Trivial_Graph_Format'>TGF</a> or <a href='https://en.wikipedia.org/wiki/DOT_%28graph_description_language%29'>DOT</a> file. Open with e.g. <a href='http://www.yworks.com/en/products/yfiles/yed/'>yED</a>.<br/>" ;
print "Example: <a href='?q=146481&props=361%2C279&doit=Generate'>Taxa</a></p>" ;

print "<form method='get' class='form inline-form form-inline'>
Root element : <input type='text' name='q' value='" . get_request('q','') . "' /><br/>
Properties to follow : <input type='text' name='props' value='" . get_request('props','') . "' /> (separate multiple with comma)<br/>
Labels : <input type='text' name='lang' value='" . get_request('lang','en') . "'  /><br/>
Format :
<label><input type='radio' name='format' value='tgf' " . ($format=='tgf'?'checked':'') . " /> Trivial Graph Format</label>
<label><input type='radio' name='format' value='dot' " . ($format=='dot'?'checked':'') . " /> DOT</label>
<br/>
<input type='submit' value='Generate Graph' name='doit' class='btn btn-primary' />
</form>" ;

if ( isset($_REQUEST['doit']) ) {
	$lang = get_request ( 'lang' , 'en' ) ;
	$q = preg_replace ( '/\D/' , '' , get_request('q','') ) ;
	$props = preg_replace ( '/[^0-9,]/' , '' , get_request('props','') ) ;
	$query = "tree[$q][][$props]" ;
	$url = $wdq_internal_url.'?q='.urlencode($query).'&props='.$props ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	
	$labels = array() ;
	foreach ( explode(',',$props) AS $i ) $labels['P'.$i] = "P$i" ;
	foreach ( $j->items AS $i ) $labels['Q'.$i] = "Q$i" ;
	$db = openDB ( 'wikidata' , 'wikidata' ) ;
	$sql = "SELECT * FROM wb_terms WHERE term_entity_type='item' and term_language='" . $db->real_escape_string($lang) . "' and term_type='label' and term_entity_id in (" . implode(',',$j->items) . ")" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		$labels['Q'.$o->term_entity_id] = $o->term_text . " (Q" . $o->term_entity_id . ")" ;
	}

	$sql = "SELECT * FROM wb_terms WHERE term_entity_type='property' and term_language='" . $db->real_escape_string($lang) . "' and term_type='label' and term_entity_id in (" . $props . ")" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		$labels['P'.$o->term_entity_id] = $o->term_text . " (P" . $o->term_entity_id . ")" ;
	}
	
	print "<textarea id='input' rows=20 style='width:100%'>" ;
	
	if ( $format == 'tgf' ) {
		foreach ( $j->items AS $i ) print "$i\t" . $labels['Q'.$i] . "\n" ;
		print "#\n" ;
		foreach ( $j->props AS $prop => $list ) {
			foreach ( $list AS $l ) {
				if ( $l[1] != 'item' ) continue ;
				if ( $l[0] == -1 || $l[2] == -1 ) continue ;
				print $l[2] . "\t" . $l[0] . "\t" . $labels['P'.$prop] . "\n" ;
			}
		}
	} else if ( $format == 'dot' ) {
		print "digraph root_is_Q$q {\n" ;
		foreach ( $j->items AS $i ) print "\tQ$i [label=\"" . $labels['Q'.$i] . "\"];\n" ;
		foreach ( $j->props AS $prop => $list ) {
			foreach ( $list AS $l ) {
				if ( $l[1] != 'item' ) continue ;
				if ( $l[0] == -1 || $l[2] == -1 ) continue ;
				print "\tQ".$l[2] . " -> Q" . $l[0] . " [label=\"" . $labels['P'.$prop] . "\"];\n" ;
			}
		}
		print "}" ;
	}
	print "</textarea>" ;

	if ( $format == 'dot' ) {
?>
<div id='output' style='overflow:auto;max-width:1000px;border:1px solid black'></div>
<script src='//mdaines.github.io/viz.js/viz.js'></script>
<script>
$(document).ready ( function () {
	var result = Viz($('#input').val(), 'svg', 'dot');
	$('#output').html(result) ;
	$('#output svg').width('100%').height('100%') ;
//	$($('#output svg g').get(0)).attr('id','viewport') ;
//	$.getScript("//www.cyberz.org/projects/SVGPan/SVGPan.js") ;
} ) ;
</script>
<?PHP
//		print "<input type='submit' value='Show online' /></form>" ;
	}
}

print get_common_footer() ;

?>