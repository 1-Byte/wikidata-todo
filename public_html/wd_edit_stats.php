<?PHP

//ini_set('memory_limit','2500M');
set_time_limit ( 60 * 10 ) ; // Seconds
include_once ( 'php/common.php' ) ;

$sparql = get_request ( 'sparql' , '' ) ;
$from = get_request ( 'from' , '' ) ;
$to = get_request ( 'to' , '' ) ;

print get_common_header ( '' , 'WD edit stats' ) ;

print "<form method='get'>
<a href='https://query.wikidata.org/' target='_blank'>SPARQL query</a> (item variable needs to be '?q'):
<textarea name='sparql'>$sparql</textarea>
<p>Start date <input type='text' name='from' value='$from' placeholder='20170801' /></p>
<p>End date <input type='text' name='to' value='$to' placeholder='20170901' /></p>
<input type='submit' name='doit' value='Do it!' class='btn btn-primary' />
</form>" ;

if ( !isset($_REQUEST['doit']) ) {
	print get_common_footer() ;
	exit ( 0 ) ;
}


$j = getSPARQLitems ( $sparql ) ;

$users = array() ;
$edits = array () ;
$sql = "SELECT rev_timestamp,rev_user,rev_user_text FROM revision WHERE rev_timestamp>='$from' AND rev_timestamp<='$to' " ;
$sql .= " AND rev_page IN (SELECT page_id FROM page WHERE page_namespace=0 AND page_title IN ('Q" . implode ( "','Q" , $j ) . "'))" ;
$db = openDB ( 'wikidata', 'wikidata' ) ;
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	$edits[substr($o->rev_timestamp,0,6)]++ ;
	if ( $o->rev_user > 0 ) {
		if ( !isset($users[$o->rev_user_text]) ) $users[$o->rev_user_text] = 0 ;
		$users[$o->rev_user_text]++ ;
	}
}
ksort( $edits ); // sort by date, ascending
arsort( $users ); // sort by count, descending

print "<h3>Edits/month</h3>" ;
print "<table class='table table-striped'><tbody>" ;
foreach ( $edits AS $date => $count ) {
	print "<tr><td><pre>$date</pre></td><td>$count</td></tr>" ;
}
print "</tbody></table>" ;

print "<h3>User edits</h3>" ;
print "<table class='table table-striped'><tbody>" ;
foreach ( $users AS $user => $count ) {
	print "<tr><td><a href='https://www.wikidata.org/wiki/User:".urlencode($user)."' target='_blank'>User:$user</a></td><td>$count</td></tr>" ;
}
print "</tbody></table>" ;

print get_common_footer() ;

?>