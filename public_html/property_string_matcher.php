<?PHP
require_once ( 'php/common.php' ) ;

$prop = get_request ( 'prop' , '' ) ;
$rows = get_request ( 'rows' , '' ) ;

print get_common_header ( '' , "Property string matcher" ) ;

print "<form method='post' class='form form-inline'>
<p>String property : <input type='text' value='$prop' name='prop' placeholder='Pxxx' /></p>
<p><textarea name='rows' rows=10 style='width:100%' placeholder='Double quotes do not work yet!'>$rows</textarea></p>
<input class='btn btn-primary' type='submit' value='Get items for string values in this property' name='doit' />
</form>" ;

if ( isset($_REQUEST['doit']) ) {
	$p = preg_replace ( '/\D/' , '' , $prop ) ;
	$prop = "P$p" ;
	$rows = explode ( "\n" , $rows ) ;
	$sub = array ( array() ) ;
	$cur = 0 ;
	foreach ( $rows AS $r ) {
		$r = trim ( $r ) ;
		if ( $r == '' ) continue ;
		if ( preg_match ( '/["\']/' , $r ) ) continue ;
		if ( count($sub[$cur]) >= 100 ) {
			$sub[] = array() ;
			$cur++ ;
		}
		$sub[$cur][] = $p.':"' . $r . '"' ;
	}

	$out = array() ;
	foreach ( $sub AS $s ) {
//		print "<pre>" ; print_r ( $s ) ; print "</pre>" ;
		$s = implode ( ',' , $s ) ;
		$url = "$wdq_internal_url?q=string" . urlencode('['.$s.']') . '&props='.$p ;
//		print "<pre>$url</pre>" ;
		$t = file_get_contents ( $url ) ;
		$t = preg_replace ( '/,"props":\{\[\]\}/' , ''  , $t ) ;
		$j = json_decode ( $t ) ;
//		print "<pre>" ; print_r ( $j ) ; print "</pre>" ;
		if ( !isset($j) or $j == null ) continue ;
		if ( !isset($j->props) ) continue ;
		if ( !isset($j->props->$p) ) continue ;
//		print "<pre>" ; print_r ( $j ) ; print "</pre>" ;
		foreach ( $j->props->$p AS $i ) {
			if ( $i[1] != 'string' ) continue ;
			$q = $i[0] ;
			$s = $i[2] ;
			$out[] = array ( $q , $s ) ;
		}
	}
	
	print "<table class='table table-condensed table-striped'><tbody>" ;
	foreach ( $out AS $o ) {
		print "<tr><td>Q" . $o[0] . "</td><td>" . $o[1] . "</td></tr>" ;
	}
	print "</tbody></table>" ;
}

print get_common_footer() ;
?>