<?PHP

include_once ( 'php/common.php' ) ;

$q = preg_replace ( '/\D/' , '' , get_request ( 'q' , '' ) ) ;
$q_award = preg_replace ( '/\D/' , '' , get_request ( 'q_award' , '' ) ) ;
$wiki = trim ( strtolower ( get_request ( 'wiki' , '' ) ) ) ;
$wikitext = trim ( get_request ( 'wikitext' , '' ) ) ;

print get_common_header('','Awarder') ;

if ( isset ( $_REQUEST['generate'] ) ) {
	$aq = explode ( ',' , get_request('aq','') ) ;
	print "<form method='post' action='./quick_statements.php'>" ;
	print "<textarea name='list' style='width:100%' rows=20>" ;
	foreach ( $aq AS $award ) {
		if ( !isset ( $_REQUEST["use_$award"] ) ) continue ;
		$year = trim ( get_request ( "year_$award" , '' ) ) ;
		$l = "Q$q\tP166\tQ$award" ;
		if ( strlen($year) == 4 ) $l .= "\tP585\t+0000000$year-01-17T00:00:00Z/9" ;
		print "$l\n" ;
	}
	print "</textarea>" ;
	print "<input type='submit' name='doit' value='Run QuickStatements' class='btn btn-primary' />" ;
	print "</form>" ;
	

	print get_common_footer() ;
	exit ( 0 ) ;
}

print "<div class='lead'>You have the Wikidata item of a person (e.g., <tt>Q44461</tt>), want to find awards the person received on Wikipedia, and add those to their Wikidata item:</div>
<form method='get' class='form form-inline inline-form'>
Wikidata item of a person with awards: <input type='text' name='q' value='$q' /> <input type='submit' name='doit' value='Do it!' class='btn btn-primary' />
</form>

<hr/>
<div class='lead'>You have a Wikipedia article about an award, with a bullet-point list of year and [[linked recipients]] (<a href='https://de.wikipedia.org/w/index.php?title=Euler-Medaille&action=edit&section=1' target='_blank'>example</a>) of that award:
</div>

<form method='post' class='form form-inline inline-form'>
List of award recipients as wikitext:
<textarea name='wikitext' placeholder='Wikitext with a bullet-point list of years and links to recipients' rows=10 style='width:100%'>$wikitext</textarea>
<input type='text' name='wiki' placeholder='Wiki, e.g. enwiki' value='$wiki' /> 
<input type='text' name='q_award' placeholder='Q of award' value='$q_award' /> 
<input type='submit' name='doit' value='Do it!' class='btn btn-primary' />
</form>" ;

if ( $q == '' and $wiki == '' ) {
	print get_common_footer() ;
	exit ( 0 ) ;
}

$db_wd = openDB ( 'wikidata' , 'wikidata' ) ;

function follow_redirects ( &$db , &$targets ) {
	
	$sql = "SELECT page_title,rd_title FROM page,redirect WHERE page_namespace=0 AND page_id=rd_from AND rd_namespace=0 AND page_title IN ('" . implode("','",$targets) . "')" ;
//	print "<pre>$sql</pre>" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
//		unset ( $targets[$o->page_title] ) ;
		$targets[$o->page_title] = $db->real_escape_string ( $o->rd_title ) ;
//		print "<pre>" ;print_r ( $o ) ; print "</pre>" ;
	}
	
}

if ( $wiki != '' and $q_award != '' ) {

	// Get qs/years we already have
	$q_award = preg_replace ( '/\D/' , '' , $q_award ) ;
	$has_year = array() ;
	$has_q = array() ;
	$sparql = "SELECT ?q (year(?date) as ?year) { ?q p:P166 ?s . ?s ps:P166 wd:Q$q_award OPTIONAL { ?s pq:P585 ?date } }" ;
	$j = getSPARQL ( $sparql ) ;
	foreach ( $j->results->bindings AS $b ) {
		if ( $b->q->type != 'uri' ) continue ;
		$q = preg_replace ( '/^.+Q/' , '' , $b->q->value ) ;
		$has_q[$q] = $q ;
		if ( !isset($b->year) ) continue ;
		if ( $b->year->type != 'literal' ) continue ;
		$year = $b->year->value ;
		$has_year[$q][$year] = $year ;
	}
	
//	print "<pre>" ; print_r ( $has_q ) ; print "</pre>" ;
//	print "<pre>" ; print_r ( $has_year ) ; print "</pre>" ;

	$db = openDBwiki ( $wiki ) ;

	$targets = array() ;
	$page2year = array() ;
	$rows = explode ( "\n" , $wikitext ) ;
	$years = array() ;
	foreach ( $rows AS $row ) {
		if ( !preg_match ( '/^\s*[\|\*\#]+\s*(\d{4})\s*(.+)$/' , $row , $m ) ) continue ;
		$year = $m[1] ;
		$row = $m[2] ;
		
		if ( !preg_match_all ( '/\[\[(.+?)\]\]/' , $row , $m ) ) continue ;
		
		$pages = $m[1] ;
		foreach ( $pages AS $page ) {
			$title = str_replace ( ' ' , '_' , $page ) ;
			$title = preg_replace ( '/_*\|.*$/' , '' , $title ) ;
			$page2year[$title] = $year ;
			$targets[$title] = $db->real_escape_string ( $title ) ;
			$years[$year]++ ;
		}
	}
	
	if ( count($targets) == 0 ) {
		print "Nothing to do!" ;
		exit ( 0 ) ;
	}
		
	follow_redirects ( $db , $targets ) ;
	$out = array() ;
	$missing = array() ;
	foreach ( $targets AS $k => $v ) {
		$sitelink = str_replace ( '_' , ' ' , $v ) ;
//		$sql = "SELECT ips_item_id FROM wb_items_per_site WHERE ips_site_id='$wiki' AND ips_site_page='$sitelink' AND EXISTS (SELECT * FROM wb_entity_per_page,pagelinks WHERE epp_entity_id=ips_item_id AND epp_entity_type='item' AND pl_from=epp_page_id AND pl_namespace=0 AND pl_title='Q5')" ;
		$sql = "SELECT ips_item_id FROM wb_items_per_site WHERE ips_site_id='$wiki' AND ips_site_page='$sitelink' AND EXISTS (SELECT * FROM page,pagelinks WHERE page_namespace=0 AND page_title=concat('Q',ips_item_id) AND pl_from=page_id AND pl_namespace=0 AND pl_title='Q5')" ;
		$q = '' ;
		$result = getSQL ( $db_wd , $sql ) ;
		while($o = $result->fetch_object()) $q = $o->ips_item_id ;
		$year = $page2year[$k] ;
		if ( $q == '' ) {
			
			if ( $years[$year] == 1 ) { // Too complex otherwise
				foreach ( $has_year AS $q_candidate => $year_candidates ) {
					if ( !isset($year_candidates[$year]) ) continue ;
					$q = $q_candidate ;
					break ;
				}
			}
			
			if ( $q == '' ) {
				$missing[] = $k ;
				continue ;
			}
		}
		
		if ( $q != '' and isset($has_year[$q]) and isset($has_year[$q][$year]) ) continue ; // No need to add that
		$out[] = "Q$q\tP166\tQ$q_award\tP585\t+$year-01-01T00:00:00Z/9" ;
	}
	
//	print "<div>Got " . count($targets) . " targets, " . count($out) . " to add, " . count($missing) . " missing.</div>" ;
	
	print "<hr/>" ;
	
	if ( count($missing) > 0 ) {
		print "<h3>Missing articles</h3>" ;
		print "<table class='table table-condensed table-striped'>" ;
		print "<thead><th>Name</th><th>Year</th><th>Search</th></thead>" ;
		print "<tbody>" ;
		foreach ( $missing AS $page ) {
			$title = str_replace ( '_' , ' ' , $page ) ;
			print "<tr>" ;
			print "<th>$title</th>" ;
			print "<td>" . $page2year[$page] . "</td>" ;
			print "<td><form class='form form-inline item-form'><input type='text' q_award='Q".$q_award."' year='" . $page2year[$page] . "' class='item' placeholder='Item number' title='Non-numerical text will be removed' />" ;
			print "<input type='submit' class='btn btn-success' value='Add' title='Adds a row to the QuickStatements box below, and removes this entry line' /></form>" ;
			print "</td>" ;
			print "<td><a href='https://www.wikidata.org/w/index.php?search=&search=".escape_attribute(urlencode(preg_replace('/\s*\(.*$/','',$title)))."&title=Special:Search&go=Go' target='_blank'>search Wikidata</a></td>" ;
			print "</tr>" ;
		}
		print "</tbody></table>" ;
	}

	print "<h3>Found matches</h3>" ;
	print "<form method='post' target='_blank' action='https://tools.wmflabs.org/wikidata-todo/quick_statements.php'>";
	print "<textarea rows=10 style='width:100%' name='list'>" . implode ( "\n" , $out ) . "</textarea>" ;
	print "<input type='submit' class='btn btn-primary'' name='doit' value='Open in QuickStatements' /> (opens in new tab)</form>" ;
	
?>

<script>
$(document).ready ( function () {
	$('form.item-form').submit ( function (event) {
		event.preventDefault();
		var item = $(this).parent().find('input.item') ;
		var q = item.val().replace(/\D/g,'') ;
		var q_award = item.attr('q_award') ;
		var year = item.attr('year') ;
		if ( q == '' ) {
			alert ( "You need to enter an item number!" ) ;
		} else {
			var s = "\nQ"+q+"\tP166\t"+q_award+"\tP585\t+"+year+"-01-01T00:00:00Z/9" ;
			var t = $.trim($('textarea[name="list"]').val()) + s ;
			$('textarea[name="list"]').val(t) ;
			$(this).parents('tr').get(0).remove() ;
		}
		return false ;
	} ) 
} ) ;
</script>

<?PHP
//	print "<pre>" ; print_r ( $targets ) ; print "</pre>" ;
	

} else if ( $q != '' ) {

	// Get awards
	$awards = implode ( ',' , getSPARQLitems ( 'SELECT ?q { ?q (wdt:P31|wdt:P279)* wd:Q618779 }' ) ) ;

//	print_r ( $awards ) ;

	// Get sites
	$wp = array() ;
	$sql = "SELECT * FROM wb_items_per_site WHERE ips_item_id=$q" ;
	$result = getSQL ( $db_wd , $sql ) ;
	while($o = $result->fetch_object()){
		$wp[] = array ( $o->ips_site_id , $o->ips_site_page ) ;
	}

	// Get existing
	$has_link = array() ;
	$sql = "select distinct pl_title FROM page,pagelinks WHERE page_title='Q$q' AND page_namespace=0 and pl_from=page_id AND pl_namespace=0" ;
	$result = getSQL ( $db_wd , $sql ) ;
	while($o = $result->fetch_object()){
		$q2 = preg_replace ( '/\D/' , '' , $o->pl_title ) ;
		$has_link[$q2] = $q2 ;
	}


	//$sitelinks = array() ;
	$award_counter = array() ;
	$aq = array() ;
	foreach ( $wp AS $wiki ) {
		$site = $wiki[0] ;
		$page = $wiki[1] ;
	//	$sitelinks[] = "<a href='//' target='_blank'>$site</a>" ;
	//	print "$site: $page<br/>" ;
	
		// Get linked pages
		$db = openDBwiki ( $site ) ;
		$p2 = $db->real_escape_string ( str_replace ( ' ' , '_' , $page ) ) ;
		$sql = "select DISTINCT pl_title FROM page,pagelinks WHERE page_namespace=0 and page_title='$p2' AND pl_from=page_id AND pl_namespace=0" ;
		$targets = array() ;
		$result = getSQL ( $db , $sql ) ;
		while($o = $result->fetch_object()){
			$title = str_replace ( '_' , ' ' , $o->pl_title ) ;
			$targets[$title] = $db_wd->real_escape_string ( $title ) ;
		}
		if ( count ( $targets ) == 0 ) continue ;
	
		$sql = "SELECT DISTINCT ips_item_id FROM wb_items_per_site WHERE ips_site_id='$site' AND ips_site_page IN ('" . implode("','",$targets) . "') AND ips_item_id IN ($awards)" ;
		$result = getSQL ( $db_wd , $sql ) ;
		while($o = $result->fetch_object()){
			$q2 = $o->ips_item_id ;
			$aq[$q2] = $q2 ;
			$award_counter[$q2]++ ;
	//		print "=> " . $o->ips_item_id . "<br/>" ;
		}
	}

	if ( count ( $aq ) == 0 ) {
		print "Could not find any awards for Q$q" ;
		print get_common_footer() ;
		exit ( 0 ) ;
	}

	$award_titles = array() ;
	$langlist = "'en','de','es','fr','it','ru'" ;
	$sql = "SELECT DISTINCT term_full_entity_id,term_text FROM wb_terms WHERE term_language IN ($langlist) AND term_type='label' AND term_full_entity_id IN ('Q" . join("','Q",$aq) . "') ORDER BY field(term_language,$langlist)" ;
	$result = getSQL ( $db_wd , $sql ) ;
	while($o = $result->fetch_object()){
		$q = preg_replace ( '/\D/' , '' , $o->term_full_entity_id ) ;
		if ( isset($award_titles[$q]) ) continue ;
		$award_titles[$q] = $o->term_text ;
	}

	print "<h2>Awards linked from Wikipedia articles for <a href='//www.wikidata.org/wiki/Q$q' target='_blank'>Q$q</a></h2>" ;
	print "<form method='post' action='?'>" ;
	print "<table class='table table-condensed table-striped'><tbody>" ;
	foreach ( $aq AS $award ) {
		$at = $award_titles[$award] ;
		if ( !isset($at) ) $at = "Q$award" ;
		print "<tr>" ;
		print "<td><input type='checkbox' name='use_$award' id='use_$award' /></td>" ; // checked
		print "<td>" . $award_counter[$award] . "&times;</td>" ;
		print "<td><a href='//www.wikidata.org/wiki/Q$award' target='_blank'>$at</a></td>" ;
		print "<td><input type='text' placeholder='Year' name='year_$award' onkeyup='$(\"#use_$award\").prop(\"checked\", true)' /></td>" ;
		if ( isset($has_link[$award]) ) print "<td>Already in item</td>" ;
		else print "<td></td>" ;
		print "</tr>" ;
	}
	print "</tbody></table>" ;
	print "<input type='hidden' name='q' value='$q' />" ;
	print "<input type='hidden' name='aq' value='" . implode(',',$aq) . "' />" ;
	print "<input type='submit' class='btn btn-primary' value='Generate statements' name='generate' />" ;
	print "</form>" ;

}

print get_common_footer() ;

?>