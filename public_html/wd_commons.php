<?PHP

//ini_set('memory_limit','2500M');
set_time_limit ( 60 * 10 ) ; // Seconds
include_once ( 'php/common.php' ) ;

$tw = 120 ;

function getCommonsFilesHTML ( $q ) {
	global $db , $tw ;
	$sql = "select * from page,iwlinks where page_id=iwl_from and iwl_prefix='wikidata' and iwl_title='" . $db->real_escape_string($q) . "'" ;
	$result = getSQL ( $db , $sql ) ;
	$h = '' ;
	while($o = $result->fetch_object()){
		if ( $o->page_namespace == 6 ) {
			$h .= "<div style='display:inline-block;margin:5px;height:$tw;width:$tw'>" ;
			$h .= "<a target='_blank' href='//commons.wikimedia.org/wiki/File:" . urlencode ( $o->page_title ) . "'>" ;
			$h .= "<img border=0 src='" . get_thumbnail_url ( "commons" , $o->page_title , $tw , "wikimedia" ) . "' /></a>" ;
			$h .= "</div>" ;
		} else if ( $o->page_namespace == 10 ) {
			$h .= "<div style='display:inline-block;text-align:center'>Category:<br/>" ;
			$h .= "<a target='_blank' href='//commons.wikimedia.org/wiki/Category:" . urlencode ( $o->page_title ) . "'>" . $o->page_title . "</a>" ;
			$h .= "</div>" ;
		}
	}
	return $h ;
}

$query = get_request ( 'query' , '' ) ;
$wdq = get_request ( 'wdq' , '' ) ;

print get_common_header ( '' , 'Wikidata Commons search' ) ;

$db = openDB ( 'commons', 'wikimedia' ) ;

//$sql = "select page_namespace,count(*) AS cnt FROM page,iwlinks where page_id=iwl_from and iwl_prefix='wikidata' group by page_namespace;" ;

print "<div class='lead'>This tool gives a small glimpse into the upcoming ability to search Wikimedia Commons using Wikidata, finding results in many languages from the latter.
<br/>This is a demo only! There are currently <~1M images on Commons associated with a Wikidata item.
<br/>Also, \"associated\" currently only means \"linked from\"; could be \"depicts\", \"made by\", \"located at\"...
<br/>So, useful searches are limited &ndash; for now!" ;
print "</div>" ;

print "<form method='get' class='form inline-form form-inline'>" ;
print "Search query : " ;
print "<input name='query' type='text' value='$query' placeholder='Any language accepted' /> " ;
print "<input type='submit' class='btn btn-primary' value='Do it!' /> <a href='?query=Burg Kidwelly'>Example use</a> <small>(Kidwelly castle, but using German \"Burg\" instead)</small>" ;
print "</form>" ;

print "<form method='get' class='form inline-form form-inline'>" ;
print "<a target='_blank' href='//wdq.wmflabs.org'>WDQ query</a> : " ;
print "<input name='wdq' type='text' value='$wdq' placeholder='WDQ query here' /> " ;
print "<input type='submit' class='btn btn-primary' value='Do it!' /> <a href='?wdq=claim[31:12042110]'>Example use</a> <small>(any steel bridge)</small>" ;
print "</form>" ;

if ( $query != '' ) {
	$url = "https://www.wikidata.org/w/api.php?action=query&list=search&format=json&srnamespace=0&srlimit=50&srsearch=" . urlencode($query) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	print "<hr/><h2>Search results</h2>" ;
	print "<div>Try the <a target='_blank' href='https://commons.wikimedia.org/w/index.php?title=Special%3ASearch&profile=advanced&search=" . urlencode($query) . "&fulltext=Search&ns0=1&ns6=1&ns9=1&ns11=1&ns14=1&ns100=1&ns106=1&profile=advanced'>same search on Commons</a></div>" ;
	foreach ( $j->query->search AS $res ) {
		$out = '' ;
		$out .= "<div><b>" ;
		$out .= "<a href='//www.wikidata.org/wiki/" . $res->title . "' target='_blank'>" . $res->title . "</a>" ;
		$out .= "</b> : " . $res->snippet . "</div>" ;
		$h = getCommonsFilesHTML ( $res->title ) ;
		if ( $h == '' ) continue ; //$h = "<i>No images for this item on Commons</i>" ;
		$out .= "<div>$h</div>" ;
		print $out ;
	}
	print "<hr/>Done!" ;
}

if ( $wdq != '' ) {
	$url = "$wdq_internal_url?q=" . urlencode ( $wdq ) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	print "<hr/><h2>Query results</h2>" ;
	foreach ( $j->items AS $q ) {
		$out = '' ;
		$out .= "<div><b>" ;
		$out .= "<a href='//www.wikidata.org/wiki/" . "Q$q" . "' target='_blank'>" . "Q$q" . "</a>" ;
		$out .= "</b>" ;
		$h = getCommonsFilesHTML ( "Q$q" ) ;
		if ( $h == '' ) continue ; //$h = "<i>No images for this item on Commons</i>" ;
		$out .= "<div>$h</div>" ;
		print $out ;
	}
	print "<hr/>Done!" ;
}

print get_common_footer() ;

?>