<?PHP

require_once ( 'php/common.php' ) ;
require_once ( 'php/pagepile.php' ) ;

$format = get_request ( 'format' , 'html' ) ;
$ts = get_request ( 'timestamp' , '' ) ;
$max = get_request ( 'max' , 100 ) * 1 ;

function print_form () {
	global $format , $ts , $max ;
	print get_common_header ( '' , 'New items' ) ;

	print "<div class='lead'>This tool can show new Wikidata items since a timestamp (or now).</div>
<form method='get' action='?' class='form form-inline'>
<p>New items before timestamp <input type='text' name='timestamp' value='$ts' placeholder='e.g.20150820122448; shorter OK' /> (leave empty for \"now\")</p>
<p>Number of items <input name='max' type='number' value='$max' /></p>
<p>Format
<label><input type='radio' name='format' value='html' ".($format=='html'?'checked':'')." /> HTML</label>
<label><input type='radio' name='format' value='pagepile' ".($format=='pagepile'?'checked':'')." /> PagePile</label>
</p>
<p><input type='submit' class='btn btn-primary' value='Do it!' name='doit' /></p>
</form>" ;
}


if ( isset ( $_REQUEST['doit'] ) ) {
	$items = array() ;
	$sql = "select * from revision where rev_parent_id=0 and rev_comment like '/* wbeditentity-create:%' " ;
	if ( $ts != '' ) $sql .= " AND rev_timestamp >= '" . ($ts*1) . "' " ;
	$sql .= " order by rev_timestamp desc limit $max" ;
	$db = openDB ( 'wikidata' , 'wikidata' ) ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		$items[$o->rev_page] = $o ;
	}
	
	$sql = "SELECT page_id,page_title FROM page WHERE page_namespace=0 AND page_id IN (" . implode(',',array_keys($items)) . ")" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		$items[$o->page_id]->page_title = $o->page_title ;
	}
	
	if ( $format == 'pagepile' ) {
		$pp = new PagePile ;
		$pp->createNewPile ( 'wikidatawiki' ) ;
		foreach ( $items AS $i ) {
			$pp->addPage ( $i->page_title , 0 ) ;
		}
		$pp->printAndEnd() ;
	} else {
		print_form () ;
		print "<table class='table table-condensed table-striped'><tbody>" ;
		foreach ( $items AS $i ) {
			print "<tr>" ;
			print "<td><a href='//www.wikidata.org/wiki/{$i->page_title}' target='_blank'>{$i->page_title}</a></td>" ;
			print "<td><a href='//www.wikidata.org/wiki/User:".myurlencode($i->rev_user_text)."' target='_blank'>{$i->rev_user_text}</a></td>" ;
			print "<td>{$i->rev_timestamp}</td>" ;
			print "</tr>" ;
		}
		print "</tbody></table>" ;
	}
} else {
	print_form () ;
}

print get_common_footer() ;

?>