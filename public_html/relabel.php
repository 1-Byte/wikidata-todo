<?PHP

require_once ( 'php/common.php' ) ;
require_once ( 'php/wikidata.php' ) ;

$labels = get_request ( 'labels' , '' ) ;
$remove_brackets = get_request ( 'remove_brackets' , 0 ) ;
$lang = get_request ( 'lang' , '' ) ;


print get_common_header ( '' , 'ReLabel' ) ;

print "<form action='?' method='post' class='form form-inline'>
<div class='lead'>Enter some strings (one per row) below, and this tool will find exact matches in Wikidata labels and aliases.</div>
<textarea name='labels' rows='5' style='width:100%'>$labels</textarea>
<div>
<label><input type='checkbox' name='remove_brackets' value='1' /> Remove brackets for search</label> | 
<span>Link labels to <input type='text' name='lang' value='$lang' />.wikipedia.org</span>
</div>
<input type='submit' value='Run' class='btn btn-primary' />
</form>" ;

if ( trim($labels) == '' ) {
	print get_common_footer() ;
	exit ( 0 ) ;
}

$db = openDB ( 'wikidata' , 'wikidata' ) ;

$rewrite = array() ;
$check = array() ;
$labels = explode ( "\n" , $labels ) ;
foreach ( $labels AS $l ) {
	$l = trim ( str_replace ( '_' , ' ' , $l ) ) ;
	if ( $l == '' ) continue ;
	if ( $remove_brackets ) {
		$l2 = preg_replace ( '/ \(.+$/' , '' , $l ) ;
		$rewrite[$l2] = $l ;
		$l = $l2 ;
	}
	$check[] = $db->real_escape_string ( $l ) ;
}

$qs = array() ;
$l2q = array() ;
$sql = "SELECT DISTINCT term_full_entity_id,term_text FROM wb_terms WHERE term_text IN ('" . implode("','",$check) . "') AND term_entity_type='item' AND term_type IN ('label','alias')" ;
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	$q = $o->term_full_entity_id ;
	$qs[$q] = $q ;
	$l2q[$o->term_text][$q] = $q ;
}


print "<table class='table table-condensed table-striped'>" ;
print "<tbody>" ;
foreach ( $l2q AS $label => $qlist ) {
	print "<tr>" ;
	print "<th valign='top'>" ;
	if ( isset($rewrite[$label]) ) $label = $rewrite[$label] ;
	if ( $lang == '' ) print $label ;
	else print "<a href='//$lang.wikipedia.org/wiki/".myurlencode($label)."' target='_blank'>$label</a>" ;
	print "</th>" ;
	print "<td>" ;
	foreach ( $qlist AS $q ) {
		print "<div><a href='//www.wikidata.org/wiki/$q' target='_blank'>$q</a></div>" ;
	}
	print "</td>" ;
	print "</tr>" ;
}
print "</tbody></table>" ;


print get_common_footer() ;

?>