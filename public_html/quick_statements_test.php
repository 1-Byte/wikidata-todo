<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

include_once ( "php/common.php" ) ;

$do_run = isset ( $_REQUEST['doit'] ) ;
$wiki = get_request ( 'wiki' , '' ) ;
$list = get_request ( 'list' , '' ) ;

print get_common_header('','QuickStatements') ;

if ( $list != '' && $wiki != '' ) {
	$db = openDB ( 'wikidata' , 'wikidata' ) ;
	$dbwp = openDBwiki ( $wiki ) ;
	$rows = explode ( "\n" , $list ) ;
	$list = array() ;
	$cache = array() ;
	foreach ( $rows AS $row ) {
		$row = explode ( "\t" , $row ) ;
		if ( count ( $row ) < 3 ) continue ;
		$article = $row[0] ;
		if ( isset($cache[$article]) ) {
			$row[0] = $cache[$article] ;
		} else {
			$oarticle = $article ;
			$row[0] = 'MISSING' ;
		
			// Check/follow redirect
			$sql = "select pl_title from page,pagelinks where page_namespace=0 and page_title='" . $dbwp->real_escape_string(str_replace(' ','_',$article)) . "' and page_is_redirect=true and pl_from=page_id and pl_namespace=0 limit 1" ;
			$result = getSQL ( $dbwp , $sql ) ;
			while($o = $result->fetch_object()){
				$article = $o->pl_title ;
			}
			$article = str_replace ( '_' , ' ' , $article ) ;

			$sql = "SELECT * FROM wb_items_per_site WHERE ips_site_id='".$db->real_escape_string($wiki)."' AND ips_site_page='".$db->real_escape_string($article)."' LIMIT 1" ;
			$result = getSQL ( $db , $sql ) ;
			while($o = $result->fetch_object()){
				$row[0] = 'Q' . $o->ips_item_id ;
			}
			$cache[$article] = $row[0] ;
			$cache[$oarticle] = $row[0] ;
		}
		
		$list[] = implode ( "\t" , $row ) ;
	}
	$list = implode ( "\n" , $list ) ;
	$wiki = '' ;
}


print "<form method='post' class='form inline-form form-inline'>
<div class='lead'>This tool can add statements (with optional qualifiers and sources) to Wikidata items.</div>" ;

/*
Each tab-separated line starts with the item to edit, followed by the property and value to set.<br/>
Qualifiers can follow as one property, one value. No duplicates will be created.<br/>
If you do not know the item Qxx values but only article titles, enter the appropriate wiki code below.<br/>
Statement and qualifier values can be either Qxx IDs, strings in \"double quotes\", or times/dates like <tt>+00000001967-01-17T00:00:00Z/11</tt> (with <tt>/11</tt> being the precision, 9=year, 11=day).<br/>
To add sources to a statement, prefix the property with <tt>S</tt> instead of <tt>P</tt> (e.g. <tt>S31</tt>).
</div>*/

print "<div>First column are articles from <input name='wiki' type='text' placeholder='e.g. enwiki' value='$wiki' />
<input type='submit' name='doit' value='Do it' class='btn btn-primary' /></div>
<div><textarea placeholder='Qxx	Pxx	value qualifier_Pxx value source_Sxx value' name='list' rows='10' style='width:100%'>$list</textarea></div>" ;

?>

<style>
.item,.prop,.string,.time,.source,.tab {
	margin:2px;
	padding:1px;
}
.item {
	background-color:#D6F8DE;
	border:3px solid #93EEAA;
}
.prop {
	background-color:#DBEBFF;
	border:3px solid #99C7FF;
}
.string {
	background-color:#FFDBFB;
	border:3px solid #FE98F1;
}
.time {
	background-color:#D7D1F8;
	border:3px solid #A095EE;
}
.source {
	background-color:#FFDFDF;
	border:3px solid #FF9797;
}
.location {
	background-color:#FFEAB7;
	border:3px solid #FFC848;
}
.tab {
	background-color:#FFFFC8;
	border:3px solid #FFFF84;
}
.value {
	background-color:#DDD;
	border:3px solid #BBB;
}
.quantity {
	background-color:#E1E1A8;
	border:3px solid #D1D17A;
}
</style>

<div style='float:right'>
<a href='#' onclick='toggleHowto();return false'>Show/hide HOWTO</a>
</div>

<div id='widar'></div><hr/>

<script>
function toggleHowto() {
	var div = document.getElementById('howto');
    if (div.style.display !== 'none') div.style.display = 'none';
    else div.style.display = 'block';
}
</script>

<div style='font-size:12pt' id='howto'>
<h3>How to use</h3>
<p>You can specify the statements to add by typing/pasting one row per statement into the above text area.
Different parts of the statement are separated by a <span class='tab'>TAB</span> character.
As <span class='tab'>TAB</span> is hard to type into a text area, you may want to use a text editor, and paste the statements into the text area later.
<br/><i>Hint:</i> You can also use a spreadsheet software such as Excel; Copying/pasting the cells should automatically insert <span class='tab'>TAB</span>s.
</p>
<p>Each statement <i>must</i> consist of an <span class='item'>item</span>, a <span class='prop'>property</span>, and a <span class='value'>value</span>.
A <span class='value'>value</span> can be another <span class='item'>item</span>, a <span class='string'>string</span>, a <span class='time'>time</span>, a <span class='location'>location</span>, or a <span class='quantity'>quantity</span>,
depending on the property type. A basic example:</p>
<p style='margin-top:20px;margin-bottom:20px;'>
<span class='item'>Q4115189</span> <span class='tab'>TAB</span> <span class='prop'>P31</span> <span class='tab'>TAB</span> <span class='item'>Q1</span>
</p>
<p>For the (initial) item of a statement, you can use an article name instead, <i>if</i> you fill in a <tt>xxwiki</tt> value in the input box above the text area; the correct item number (if available) will be retrieved automatically.</p>
<ul>
<li><span class='item'>Items</span> are always in the form <tt>Qxx</tt>, <span class='prop'>properties</span> in the form <tt>Pxx</tt>.<br/>&nbsp;</li>
<li><span class='string'>Strings</span> <i>have</i> to be <tt>"in double quotes"</tt>.<br/>
For a monolingual string, prefix it with the language and a colon, e.g. <tt>en:"Some text"</tt><br/>&nbsp;</li>
<li><span class='time'>Time</span> values <i>must</i> have the  <tt>+1967-01-17T00:00:00Z/11</tt>, with the <tt>/11</tt> designating the precision.<br/>
A precision of 11=day, 10=month, 9=year; default is 9.<br/>&nbsp;</li>
<li><span class='location'>Location</span> in the form of <tt>@LAT/LON</tt>, with LAT and LON as decimal numbers.<br/>&nbsp;</li>
<li><span class='quantity'>Quantity</span> in the form of <tt>[+-]d*</tt>, with <tt>d</tt> being a digit.</li>
</ul>
<p>Each statement "triplet" can be followed by an unlimited number of "qualifiers pairs" of <span class='prop'>property</span> <span class='tab'>TAB</span> <span class='value'>value</span>.</p>
<p>Each statement can be followed by an unlimited number of "source pairs" of <span class='source'>source property</span> <span class='tab'>TAB</span> <span class='value'>value</span>.<br/>
The source property is identical to the "normal" property, except it uses the form <tt>Sxx</tt> instead of <tt>Pxx</tt>.</p>
<p><i>Note:</i> Existing statements with an exact match (property <i>and</i> value) will <i>not</i> be added again.</p>
<p>A valid example statement would thus be:</p>
<p><span class='item'>Q4115189</span> <span class='tab'>TAB</span> <span class='prop'>P31</span> <span class='tab'>TAB</span> <span class='item'>Q1</span>
<span class='tab'>TAB</span> <span class='prop'>P580</span> <span class='tab'>TAB</span> <span class='time'>+1840-01-01T00:00:00Z/09</span>
<span class='tab'>TAB</span> <span class='source'>S143</span> <span class='tab'>TAB</span> <span class='item'>Q48183</span>
</p>
<p style='font-size:9pt'>("Sandbox item" is an "instance of" "universe", qualifier "start date"=1840, sourced as "imported from" "German Wikipedia")</p>
<h4>Item creation and labels/alias/description/sitelink</h4>
<p>To add a label in a specific language to an item, use "Lxx" instead of the property, with "xx" as the language code.</p>
<p>To add an alias in a specific language to an item, use "Axx" instead of the property, with "xx" as the language code.</p>
<p>To add a description in a specific language to an item, use "Dxx" instead of the property, with "xx" as the language code.</p>
<p>To add a sitelink to a specific page on a site to an item, use "Sxxx" instead of the property, with "xxx" as the site (e.g. <i>enwiki</i>).</p>
<p>For example, <span class='prop'>Lfr</span> would add a label in French.</p>
<p>These values <b>must</b> be <span class='string'>strings</span> in double quotes.</p>
<p>You can also create new items by inserting a line consisting only of the word "CREATE".<br/><i>PLEASE ENSURE YOU DO NOT CREATE DUPLICATE ITEMS!</i></p>
<p>To add statements to the newly created item, use the word "LAST" instead of the <span class='item'>Q number</span>; the statement will be added to the last created item.</p>
<p>An example for creating a new item and setting a label:</p>
<p><span class='value'>CREATE</span></p>
<p><span class='item'>LAST</span> <span class='tab'>TAB</span> <span class='prop'>Lfr</span> <span class='tab'>TAB</span> <span class='string'>"Le croissant magnifique!"</span></p>
<h4>Item merging</h4>
<p>You can merge two items. The first item will be merged and (if successful) redirected into the second item.</p>
<p><span class='value'>MERGE</span> <span class='tab'>TAB</span> <span class='item'>Qsource</span> <span class='tab'>TAB</span> <span class='item'>Qdestination</span></p>
<hr/>
</div>


<?PHP



/*
<div>Each line must be in the following form:<br/>
<tt style='font-size:9pt'>Article/Item TAB Property TAB value [ TAB qualifier_property TAB qualifier_value ]* [TAB source_property TAB source_value]*</tt>
<br/>Items need to be prefixed with \"Q\", string values need to be enclosed in double quotes. Other value types are not supported yet.
</div>
</form>
*/


if ( $do_run ) {
	$rows = explode ( "\n" , $list ) ;
	print "<script>toggleHowto();</script>" ;
	print "<div id='out'></div>\n\n<script>\nvar rows = " . json_encode(str_replace("\r","",$rows)) . ";\n</script>\n" ;
} else {
	print "<script>\nvar rows = [];\n</script>\n" ;
}

?>


<script>
$(document).ready ( function () {
	$.getJSON ( 'https://tools.wmflabs.org/widar/?action=get_rights&botmode=1' , function (d) {
		if ( typeof d.result.error != 'undefined' ) {
			$('#widar').html ( "You need to authorize <a href='/widar/index.php?action=authorize' target='_blank'>WiDaR</a> to edit Wikidata on you behalf for this tool to work!" ) ;
		} else {
			var u = d.result.query.userinfo.name ;
			$('#widar').html ( "You are logged into WiDaR as <a href='https://www.wikidata.org/wiki/Special:Contributions/" + encodeURIComponent(u) + "' target='_blank'>" + u + "</a>." ) ;
		}
	} ) ;
} ) ;
</script>

<script>

function getParams ( q , p , v ) {
	if ( !q.match(/^P\d+/) )  q = 'Q'+(''+q).replace(/\D/g,'') ;
	p = 'P'+(''+p).replace(/\D/g,'') ;
	var params ;
	var m = v.match ( /^"(.+)"$/ ) ;
	if ( m != null ) {
		return { botmode:1 , action:'set_string' , id:q , prop:p , text:m[1] } ;
	}

	var m = v.match ( /^([a-zA-Z-_]+):"(.+)"$/ ) ;
	if ( m != null ) {
		return { botmode:1 , action:'set_monolang' , id:q , prop:p , language:m[1] , text:m[2] } ;
	}
	
	m = v.match ( /^\s*(Q\d+)\s*/i ) ;
	if ( m != null ) {
		return { botmode:1 , action:'set_claims' , ids:q , prop:p , target:m[1] } ;
	}

	m = v.match ( /^\s*@(-{0,1}[0-9.]+)\/(-{0,1}[0-9.]+)\s*/i ) ;
	if ( m != null ) {
		return { botmode:1 , action:'set_location' , id:q , prop:p , lat:m[1] , lon:m[2] } ;
	}

	m = v.match ( /^\s*([+-]\S+T\S+Z\S*)\s*/i ) ;
	if ( m != null ) {
		d = m[1].split('/') ;
		if ( d.length == 1 ) d.push('9') ; // Precision appended with "/"; default:year
		return { botmode:1 , action:'set_date' , id:q , prop:p , date:d[0] , prec:d[1] } ;
	}

	m = v.match ( /^\s*([+-]{0,1}\d+)\s*/i ) ;
	if ( m != null ) {
		d = m[1].split('/') ;
//		if ( d.length == 1 ) d.push('9') ; // Precision appended with "/"; default:year
		return { botmode:1 , action:'set_quantity' , id:q , prop:p , amount:d[0] } ; //  , unit:1
	}
	
	return params ;
}

function getSourceParams ( q , p , v ) {
	q = 'Q'+(''+q).replace(/\D/g,'') ;
	p = 'P'+(''+p).replace(/\D/g,'') ;
	var params ;
	var m = v.match ( /^"(.+)"$/ ) ;
	if ( m != null ) {
		var ret = {} ;
		ret[p] = [{
				snaktype:"value",
				property:p,
				datavalue:{
					type:"string",
					value:m[1]
				}
			}] ;
		return ret ;
	}
	
	m = v.match ( /^\s*(Q\d+)\s*/i ) ;
	if ( m != null ) {
		var ret = {} ;
		ret[p] = [{
				snaktype:"value",
				property:p,
				datavalue:{
					type:"wikibase-entityid",
					value:{
						"entity-type": "item",
						"numeric-id":m[1].replace(/\D/g,'')*1
					}
				}
			} 
		] ;
		return ret ;
	}

	m = v.match ( /^\s*([+-]0\S+)\s*/i ) ;
	if ( m != null ) { // TODO
		d = m[1].split('/') ;
//		if ( d.length == 1 ) d.push('9') ; // Precision appended with "/"; default:year
//		return { botmode:1 , action:'set_date' , id:q , prop:p , date:d[0] , prec:d[1] } ;
	}
	
	return null ;
}

var max_concurrent = 1 ;
var running = 0 ;
var widar_api = '/widar/index.php' ;

function getWidar ( params , callback ) {
	params.tool_hashtag = 'quickstatements' ;
	$.get ( widar_api , params , function ( d ) {
		if ( d.error != 'OK' ) {
			console.log ( params ) ;
			if ( null != d.error.match(/Invalid token/) || null != d.error.match(/happen/) || null != d.error.match(/Problem creating item/) || ( params.action!='create_redirect' && null != d.error.match(/failed/) ) ) {
				console.log ( "ERROR (re-trying)" , params , d ) ;
				setTimeout ( function () { getWidar ( params , callback ) } , 500 ) ; // Again
			} else {
				console.log ( "ERROR (aborting)" , params , d ) ;
				var h = "<li style='color:red'>ERROR (" + params.action + ") : " + d.error + "</li>" ;
				$('#out ol').append(h) ;
				callback ( d ) ; // Continue anyway
			}
		} else {
			callback ( d ) ;
		}
	} , 'json' ) . fail(function() {
		console.log ( "Again" , params ) ;
		getWidar ( params , callback ) ;
	}) ;
}

function addQualifiers ( row , claim ) {

	if ( row.length == 3 ) {
		running-- ;
		doNextOne() ;
		return ;
	}

	var val = row.pop() ;
	var p = row.pop() ;
	
	
	if ( null != p.match ( /^S\d+/ ) ) {
	
		p = 'P'+(''+p).replace(/\D/g,'') ;
		var statement = claim.id ;
		
		var snaks = getSourceParams ( row[0] , p , val ) ;
		
		if ( snaks == null ) {
			addQualifiers ( row , claim ) ;
			return ;
		}
		
		
		function doThat1 () {
			getWidar ( {
				action:'add_source',
				statement:statement,
				snaks:JSON.stringify(snaks),
				botmode:1
			} , function ( d2 ) {
				addQualifiers ( row , claim ) ;
			} ) ;
		}
		
		doThat1() ;
		
	
	} else {
		p = 'P'+(''+p).replace(/\D/g,'') ;
	
		var exists = false ;
		$.each ( (claim.qualifiers||[]) , function ( k , v ) {
			if ( k == p ) exists = true ;
		} ) ;
		if ( exists ) {
			addQualifiers ( row , claim ) ; // Next one
			return ;
		}
	
		var params2 = getParams ( row[0] , p , val ) ;
		params2.claim = claim.id ;
		
		function doThat2() {
			getWidar ( params2 , function ( d2 ) {
				addQualifiers ( row , claim ) ;
			} ) ;
		}
		
		doThat2() ;
		
	}
}

function setLabel ( q , lang , value ) {
	getWidar ( {
		action : 'set_label' ,
		q : (q+'').replace('/\D/g','') ,
		label : value.replace(/^"/,'').replace(/"$/,'') ,
		lang : lang ,
		botmode : 1
	} , function ( d ) {
//		console.log ( d ) ;
		running-- ;
		doNextOne() ;
	} ) ;
}

function setDesc ( q , lang , value ) {
	getWidar ( {
		action : 'set_desc' ,
		q : (q+'').replace('/\D/g','') ,
		label : value.replace(/^"/,'').replace(/"$/,'') ,
		lang : lang ,
		botmode : 1
	} , function ( d ) {
//		console.log ( d ) ;
		running-- ;
		doNextOne() ;
	} ) ;
}

function set_Alias ( q , lang , value ) {
	getWidar ( {
		action : 'set_alias' ,
		q : (q+'').replace('/\D/g','') ,
		label : value.replace(/^"/,'').replace(/"$/,'') ,
		lang : lang ,
		mode : 'add' ,
		botmode : 1
	} , function ( d ) {
//		console.log ( d ) ;
		running-- ;
		doNextOne() ;
	} ) ;
}

// https://tools.wmflabs.org/widar/index.php?action=set_sitelink&q=Q6474469&site=eowiki&title=Lajos+Tak%C3%A1cs
function set_Site ( q , site , value ) {
//	console.log ( (q+'').replace('/\D/g','') , site , value ) ;
	getWidar ( {
		action : 'set_sitelink' ,
		q : (q+'').replace('/\D/g','') ,
		title : value.replace(/^"/,'').replace(/"$/,'') ,
		site : site ,
		botmode : 1
	} , function ( d ) {
//		console.log ( d ) ;
		running-- ;
		doNextOne() ;
	} ) ;
}



var last_q = '' ;

function createNewItem () {
	getWidar ( {
		action:'create_blank_item',
		botmode:1
	} , function ( d ) {
		if ( typeof d.q == 'undefined' ) {
			createNewItem() ;
			return ;
		}
		last_q = d.q ;
		running-- ;
		doNextOne() ;
	} ) ;
}



function redirectItem ( from , to ) {
	getWidar ( {
		action:'create_redirect',
		from:'Q'+(''+from).replace(/\D/g,''),
		to:'Q'+(''+to).replace(/\D/g,''),
		botmode:1
	} , function ( d ) {
//		console.log ( d ) ;
		var h = "<li>Merged " + from + " into " + to + ".</li>" ;
		$('#out ol').append(h) ;
		running-- ;
		doNextOne() ;
	} ) ;
}


function mergeItems ( from , to ) {
	getWidar ( {
		action:'merge_items',
		from:'Q'+(''+from).replace(/\D/g,''),
		to:'Q'+(''+to).replace(/\D/g,''),
		botmode:1
	} , function ( d ) {
		if ( d.error != 'OK' ) {
			var h = "<li>Error while merging " + from + " into " + to + ": " + d.error + "</li>" ;
			$('#out ol').append(h) ;
			running-- ;
			doNextOne() ;
			return ;
		}
		redirectItem ( from , to ) ;
	} ) ;
}


function doNextOne() {
//	console.log ( "NEXTONE" , rows.length , running ) ;
	if ( rows.length == 0 ) {
		if ( running == 0 ) {
			var h = "<hr/><div>All done!.</div>" ;
			$('#out').append(h) ;
		}
		return ;
	}
	if ( running >= max_concurrent ) return ;
	running++ ;
	
	var row = $.trim(rows.shift()) ;
	
	if ( row == '' ) {
		running-- ;
		doNextOne() ;
		return ;
	}
	
	if ( row.toLowerCase() == 'create' ) {
		createNewItem() ;
		return ;
	}
	
	row = row.split("\t") ;
	
	if ( row[0].toUpperCase() == 'MERGE' ) {
		mergeItems ( row[1] , row[2] ) ;
		return ;
	}
	
	
	if ( row[0] == 'MISSING' ) {
		var h = "<li>Skipping an item that could not be found.</li>" ;
		$('#out ol').append(h) ;
		running-- ;
		doNextOne() ;
		return ;
	}


	if ( $.trim(row[0]).toLowerCase() == 'last' ) {
		if ( last_q == '' ) {
			var h = "<li>No LAST item defined!.</li>" ;
			$('#out ol').append(h) ;
			running-- ;
			doNextOne() ;
			return ;
		}
		console.log ( "Replacing LAST with " + last_q ) ;
		row[0] = last_q ;
	}

	if ( $.trim(row[2]).toLowerCase() == 'last' ) {
		if ( last_q == '' ) {
			var h = "<li>No LAST item defined!.</li>" ;
			$('#out ol').append(h) ;
			running-- ;
			doNextOne() ;
			return ;
		}
		console.log ( "Replacing LAST with " + last_q ) ;
		row[2] = last_q ;
	}
	
	if ( null == row[0].match(/^[PQ]\d+/) ) {
		var h = "<li>ERROR: " + row[0] + " is not a Wikidata item (Qxxx). Did you forget to set a wiki to convert from articles to items?</li>" ;
		$('#out ol').append(h) ;
		running-- ;
		doNextOne() ;
		return ;
	}
	
	last_q = row[0] ;
	
	var h = "<li>Processing <a href='https://www.wikidata.org/wiki/" + row[0] + "' target='_blank'>" + row[0] + "</a> (" + row.join(" ") + ")</li>" ;
	$('#out ol').append(h) ;
	
	var m = row[1].match ( /^L(.+)$/ ) ;
	if ( m != null ) {
		setLabel ( row[0] , m[1].toLowerCase() , row[2] ) ;
		return ;
	}

	var m = row[1].match ( /^D(.+)$/ ) ;
	if ( m != null ) {
		setDesc ( row[0] , m[1].toLowerCase() , row[2] ) ;
		return ;
	}

	var m = row[1].match ( /^A(.+)$/ ) ;
	if ( m != null ) {
		set_Alias ( row[0] , m[1].toLowerCase() , row[2] ) ;
		return ;
	}

	var m = row[1].match ( /^S(.+)$/ ) ;
	if ( m != null ) {
		set_Site ( row[0] , m[1].toLowerCase() , row[2] ) ;
		return ;
	}
	
	
	var params = getParams ( row[0] , row[1] , row[2] ) ;
	if ( typeof params == 'undefined' ) {
		console.log ( "ERROR" , row ) ;
		running-- ;
		doNextOne() ;
		return ;
	}

	function doMain() {
//console.log(params);
		getWidar ( params , function ( d ) {
//console.log(d);
			if ( row.length == 3 ) { // No qualifiers
				running-- ;
				doNextOne() ;
				return ;
			}
		
			var q = params.id||params.ids ;
			var claim = (d.res||{}).claim||{} ;

			if ( typeof claim.id == 'undefined' ) {
				$.getJSON ( 'https://www.wikidata.org/w/api.php?callback=?' , {
					action:'wbgetentities',
					ids:q,
					format:'json'
				} , function ( d2 ) {
					var p = params.prop ;
					
					var found = false ;
					$.each ( ((((d2.entities[q])||{}).claims||{})[p]||[]) , function ( k , v ) {
						var type = v.mainsnak.datatype ;
						if ( type == 'wikibase-item' ) {
							if ( 'Q'+v.mainsnak.datavalue.value['numeric-id'] != params.target ) return ;
						} else if ( type == 'string' ) {
							if ( v.mainsnak.datavalue.value != params.text ) return ;
						} else if ( type == 'time' ) {
							var t1 = v.mainsnak.datavalue.value.time ;
							var t2 = params.date ;
							t1 = t1.replace ( /^(.)0+/ , '$1' ) ;
							t2 = t2.replace ( /^(.)0+/ , '$1' ) ;
//							console.log ( t1 , t2 ) ; return ;
							if ( t1 != t2 ) return ;
						} else if ( type == 'globe-coordinate' ) {
							var factor = 1000000000000 ;
							var lat = Math.round(params.lat*factor)/factor ;
							var lon = Math.round(params.lon*factor)/factor ;
//							console.log ( v.mainsnak.datavalue.value ) ;
//							console.log ( lat,lon ) ;
							if ( v.mainsnak.datavalue.value.latitude != lat || v.mainsnak.datavalue.value.longitude != lon ) return ;
						} else if ( type == 'monolingualtext' ) {
							if ( v.mainsnak.datavalue.value['text'] != params.text || v.mainsnak.datavalue.value['language'] != params.language ) return ;
						} else if ( type == 'quantity' ) {
							if ( v.mainsnak.datavalue.value['amount']*1 != params.amount*1 || v.mainsnak.datavalue.value['unit'] != "1" /* actual units not yet supported */ ) return ;
						} else { // Unknown type
							console.log ( "UNKNOWN TYPE " + type ) ;
							console.log ( v.mainsnak.datavalue.value ) ;
							console.log ( params ) ;
							return ;
						}
						found = true ;
						claim = v ;
						addQualifiers ( row , claim ) ;
						return false ;
	//					console.log ( "EXISTING" , v ) ;
					} ) ;
					if ( !found ) {
						running-- ;
						doNextOne() ;
					}
				
				} ) ;
			} else {
				addQualifiers ( row , claim ) ;
			}
		
		
		} ) ;
	}
		
	doMain() ;
}

// Aaaand ... go!
$(document).ready ( function () {
	$('#out').append ( '<ol>' ) ;
	for ( var i = 0 ; i < max_concurrent ; i++ ) doNextOne() ;
} ) ;

</script>

<?PHP

print get_common_footer() ;

?>