<?PHP

ini_set('memory_limit','2500M');
set_time_limit ( 60 * 10 ) ; // Seconds
include_once ( 'php/common.php' ) ;

header('Content-type: text/plain; charset=utf-8');

$prop = get_request ( 'prop' , '0' ) * 1 ; //990;//227 ;
if ( $prop == 0 ) {
	print "Usage:\n\n\thttp://tools.wmflabs.org/wikidata-todo/translate_items_with_property.php?prop=PROPNUM\n\nwith PROPNUM a numerical property\n" ;
	exit ( 0 ) ;
}

$items = array() ;
$item2id = array() ;
$j = getSPARQL ( "SELECT ?q ?pv { ?q wdt:P$prop ?pv }" ) ;
foreach ( $j->results->bindings AS $k => $v ) {
	if ( $v->pv->type != 'literal' ) continue ;
	$q = preg_replace ( '/^.+entity\/Q/' , '' , $v->q->value ) ;
	$item2id[$q] = $v->pv->value ;
}
unset ( $j ) ;

$db = openDB ( 'wikidata' , 'wikidata' ) ;
$sql = "select term_entity_id AS q,term_language AS lang,term_text as text,term_type AS type from wb_terms where term_entity_type='item' and term_type IN ('label','alias') and term_entity_id IN (" . implode(',',array_keys($item2id)) . ")" ;
$result = getSQL ( $db , $sql ) ;
print "Item\tID\tLanguage\tType\tText\n" ;
while($o = $result->fetch_object()){
	if ( !isset ( $item2id[$o->q] ) ) continue ; // Paranoia
	print "Q" . $o->q . "\t" . $item2id[$o->q] . "\t" . $o->lang . "\t" . $o->type . "\t" . $o->text . "\n" ;
}

?>