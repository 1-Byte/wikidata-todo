<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

include_once ( "php/common.php" ) ;

$query = get_request ( 'q' , '' ) ;
$botmode = isset ( $_REQUEST['botmode'] ) ;
$callback = get_request ( 'callback' , '' ) ;


if ( $botmode ) {
	header('Content-type: application/json');
} else {
	print get_common_header ( '' , "Related properties" ) ;

	print "<div class='lead'>This tool can analyze the results from a <a href='http://wdq.wmflabs.org/'>WikiData Query</a>, and show the properties used in all items matching a query.</div>" ;

	print '
	<form class="form-horizontal">
		<div class="control-group">
			<label class="control-label" for="q">WikiData Query</label>
			<div class="controls">
				<input type="text" id="q" name="q" placeholder="" value="'.$query.'">
				<small>(<a href="./related_properties.php?q=claim[31%3A2334719]">Example</a>: all instances of court cases)</small>
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				<button type="submit" class="btn btn-primary">Do it</button>
			</div>
		</div>
	</form>' ;
}

$max_items = 50000 ;

if ( $query != '' ) {
	$wdq = json_decode ( file_get_contents ( $wdq_internal_url.'?q='.urlencode($query) ) ) ;
	
	if ( count ( $wdq->items ) > $max_items ) {
		shuffle ( $wdq->items ) ;
		while( count ( $wdq->items ) > $max_items ) array_pop ( $wdq->items ) ;
	}
	
	$db = openDB ( 'wikidata' , '' ) ;
	$sql = "select pl_title as property,count(*) as cnt from page,pagelinks where pl_namespace=120 and pl_from=page_id and page_namespace=0 and page_title IN ('Q".implode("','Q",$wdq->items)."') group by pl_title order by cnt desc" ;
	$result = getSQL ( $db , $sql ) ;
	$prop_ids = array() ;
	$props = array() ;
	$total = count ( $wdq->items ) ;
	while($o = $result->fetch_object()){
		$props[] = $o ;
		$prop_ids[] = substr ( $o->property , 1 ) ;
	}

	if ( $botmode ) {
	
		if ( $callback != '' ) print "$callback(" ;
		print json_encode ( $props ) ;
		if ( $callback != '' ) print ")" ;
	
	} else {

		$sql = "select term_full_entity_id as prop,term_text as label from wb_terms where term_type='label' and term_language='en' and term_entity_type='property' and term_full_entity_id in ('P".implode("','P",$prop_ids)."')" ;
		$result = getSQL ( $db , $sql ) ;
		$labels = array() ;
		while($o = $result->fetch_object()){
			$labels[$o->prop] = $o->label ;
		}
	//	print "<pre>" ; print_r ( $labels ) ; print "</pre>" ;

		print "<table class='table-condensed table-striped'>" ;
		print "<thead><tr><th>Property</th><th>Occurrences</th><th>% total</th><th>Subgroup</th></tr></thead><tbody>" ;
		foreach ( $props AS $p ) {
			print "<tr>" ;
			print "<td><a target='_blank' href='//www.wikidata.org/wiki/Property:" . $p->property . "'>" ;
			if ( isset ( $labels[$p->property] ) ) print $labels[$p->property] . "</a> (" . $p->property . ")" ;
			else print $p->property . "</a>" ;
			print "</td>" ;
			print "<td style='text-align:right;font-family:courier'>" . number_format ( $p->cnt ) . "</td>" ;
			print "<td style='text-align:right;font-family:courier'>" . number_format ( 100*$p->cnt/$total , 1 ) . " %</td>" ;
			print "<td><a target='_blank' href='/wikidata-todo/autolist.html?q=".$query.' and claim['.substr($p->property,1).']'."'>Items</a></td>" ;
			print "</tr>" ;
		}
		print "</tbody></table>" ;
	}
}


if ( !$botmode ) print get_common_footer() ;

?>