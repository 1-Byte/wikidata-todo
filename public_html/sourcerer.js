var wd_sourcerer = {

	init : function () {
		var self = this ;
 		$('body').append ( "<div id='sourcerer-dialog' style='display:none'></div>" ) ;
		var portletLink = mw.util.addPortletLink( 'p-tb', 'javascript:;', 'Sourcerer','t-wd_sourcerer');
		$(portletLink).click ( function () {
			self.run() ;
			return false ;
		} ) ;
//		setTimeout ( function(){self.run()} , 500 ) ; // TESTING
	} ,
	
	run : function () {
		$('#sourcerer').remove() ;
		var h = '<div id="sourcerer" style="clear:both"><i>Checking external links...</i></div>' ;
		$($('#sitelinks-wikipedia').get(0)).before ( h ) ;
		var self = this ;
		self.urls = {} ;
		self.todo = 0 ;
		$('td.wb-sitelinks-link a').each ( function ( k , v ) {
			self.todo++ ;
			var m = $(v).attr('href').match ( /^\/*(.+?)\/wiki\/(.+)$/ ) ;
			var proj = m[1] ;
			var page = m[2] ;
			$.getJSON ( 'https://'+proj+'/w/api.php?callback=?' , {
				action:'parse',
				format:'json',
				prop:'externallinks',
				page:page
			} , function ( d ) {
				if ( d.parse === undefined || d.parse.externallinks === undefined ) return self.show() ;
				
				$.each ( d.parse.externallinks , function ( k , url ) {
					if ( self.urls[url] === undefined ) self.urls[url] = [] ;
					self.urls[url] .push ( proj ) ;
				} ) ;
				
				self.show() ;
			} ) ;
		} ) ;
	} ,

	show : function () {
		var self = this ;
		self.todo-- ;
		if ( self.todo > 0 ) return ; // Not yet
		
		var keys = [] ;
		$.each ( self.urls , function ( k , v ) { keys.push ( k ) } ) ;
		keys = keys.sort ( function ( a , b ) {
			return ( (self.urls[b].length) - (self.urls[a].length) ) ;
		} ) ;
		
		var h = '' ;
		h += "<table cellspacing='0' cellpadding='2' border='1px solid #DDDDDD'><thead><tr><th>URL</th><th>Link</th><th>Count</th><th>Projects</th></tr></thead><tbody>";
		$.each ( keys , function ( dummy , url ) {
			h += "<tr><td nowrap><small>" + url + "</small></td><td><a target='_blank' href='" + url + "'>link</a></td><td>" + self.urls[url].length + "</td><td>" + self.urls[url].sort().join(', ') + "</td></tr>" ;
		} ) ;
		h += "</tbody></table>";
		$('#sourcerer').html ( h )
	} ,

	fin : ''

} ;

addOnloadHook ( function() {
	if ( mw.config.get('wgNamespaceNumber') != 0 ) return ;
	if ( mw.config.get('wgAction') != 'view' ) return ;
 
	wd_sourcerer.init () ;
});