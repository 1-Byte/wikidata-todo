importScriptURI("//tools.wmflabs.org/wikidata-todo/autodesc_test.js");

$(document).ready ( function () {

	var tab_width = 7 ;
	var tab_color = 'rgb(6, 69, 173)' ;
	var site = 'enwiki' ;

	function loadExplanation ( a , title ) {
		wd_auto_desc.loadItemForPage ( site , title , {
			links : 'wikipedia' ,
			local_links : true ,
			fallback : 'manual_desc' ,
			callback : function ( q , html , opt ) {
				$("#bv_"+opt.q).remove() ;
				a.after ( "<span id='bv_"+opt.q+"'> ["+html+" <sup><a href='//www.wikidata.org/wiki/"+opt.q+"' class='external text'>Q</a></sup>]</span>" ) ;
				process();
			}
		} ) ;
	}


	function process ( base ) {
		$('#mw-content-text a').not('.bv').not('.external').not('.image').not('.mw-editsection-visualeditor').mouseenter ( function () {
			var a = $(this) ;
			var href = a.attr('href') ;
			var m = href.match(/^\/wiki\/(.+)$/) ;
			if ( m == null ) return ; // Non-wiki
			var title = m[1] ;
		
			var w = parseInt(a.width());
			var h = parseInt(a.height());
			var p = a.position() ;

			var html = '' ;
			html += "<div id='bv_marker' style='border-right:"+tab_width+"px solid "+tab_color+";z-index:5;top:"+(p.top-1)+"px;left:"+(p.left-2)+"px;width:"+(w+1)+"px;height:"+(h+1)+"px;position:absolute'></div>" ;
			// border:1px solid "+tab_color+";
		
			a.addClass('bv').prepend ( html ) ;
		
			$('#bv_marker').css({'opacity':'0.5'}).click ( function ( e ) {
				var o = a.offset() ;
				if ( e.pageX >= o.left+w-0 && e.pageX <= o.left+w+tab_width ) {
					e.preventDefault();
					e.stopPropagation();
					loadExplanation ( a , unescape(title) ) ;
					return false ;
				} else {
					return true ;
				}
			} ) ;
		
		} ) . mouseleave ( function () {
	
			$('#bv_marker').remove() ;
	
		} ) ;
	}
	
	process () ;
} ) ;