<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // |E_ALL
ini_set('display_errors', 'On');

$test = isset($_REQUEST['test']) ;

require_once ( 'php/common.php' ) ;

header("Connection: close");
header('Content-type: text/plain; charset=UTF-8');
header("Cache-Control: no-cache, must-revalidate");


class WikidataDescriptionEn {

	function __construct () {
		$this->nationality = array('Ecuador'=>'Ecuadorian','Ghana'=>'Ghanaian','Russia'=>'Russian','Argentina'=>'Argentine','Australia'=>'Australian','Austria'=>'Austrian','Belgium'=>'Belgian','Brazil'=>'Brazilian','Canada'=>'Canadian','Chile'=>'Chilean','China'=>'Chinese','Denmark'=>'Danish','Finland'=>'Finnish','Faroe Islands'=>'Faroese','Netherlands'=>'Dutch','Puerto Rico'=>'Puerto Rican','France'=>'French','Luxembourg'=>'Luxembourgish','Germany'=>'German','Greece'=>'Greek','Holland'=>'Dutch','Hungary'=>'Hungarian','Iceland'=>'Icelander','India'=>'Indian','Iran'=>'Iranian','Iraq'=>'Iraqi','Ireland'=>'Irish','Israel'=>'Israeli','Indonesia'=>'Indonesian','Italy'=>'Italian','Japan'=>'Japanese','Jamaica'=>'Jamaican','Jordan'=>'Jordanian','Mexico'=>'Mexican','Nepal'=>'Nepalese','New Zealand'=>'New Zealander','Norway'=>'Norwegian','Pakistan'=>'Pakistani','Paraguay'=>'Paraguayan','Peru'=>'Peruvian','Poland'=>'Polish','Romania'=>'Romanian','Scotland'=>'Scottish','South Africa'=>'South African','Spain'=>'Spanish','Switzerland'=>'Swiss','Syria'=>'Syrian','Thailand'=>'Thai','Turkey'=>'Turkish','USA'=>'American','Uruguay'=>'Uruguayan','Venezuela'=>'Venezuelan','Wales'=>'Welsh','United Kingdom'=>'British','United States of America'=>'US-American','Sweden'=>'Swedish') ;
	}

	function collapseList ( $list ) {
		$ret = '' ;
		if ( count ( $list ) == 2 ) {
			$ret = $list[0] . ' and ' . $list[1] ;
		} else {
			if ( count ( $list ) > 2 ) $list[count($list)-1] = 'and ' . $list[count($list)-1] ; // Oxford comma!
			$ret = implode ( ', ' , $list ) ;
		}
		return $ret ;
	}

	function getObjectDescription ( $d ) {
		$ret = array() ;
		
		$arr = $d['object'] ;
		if ( !is_array($d['object']) ) $arr = array($d['object']) ;
		foreach ( $arr AS $a ) {
			$ret[] = $this->wd->renderLink($a) ;
		}
	
		$ret = $this->collapseList ( $ret ) ;
	
		if ( isset ( $d['attr'] ) ) {
			$al = array() ;
			foreach ( $d['attr'] AS $a ) {
				$l = $this->wd->getLabel ( $a['q'] ) ;
				if ( $a['p'] == 'P27' ) {
					if ( isset($this->nationality[$l]) ) $l = $this->wd->renderLink ( $a['q'] , $this->nationality[$l] ) ;
					else $l = $this->wd->renderLink($a['q']) ;
				} else {
					$l = $this->wd->renderLink($a['q']) ; ;
				}
				$al[] = $l ;
			}
			if ( count($al) > 0 ) $ret = implode ( ' ' , $al ) . ' ' . $ret ;
		}
	
	
		return $ret ;
	}


	function renderML ( $ml ) {
		$ret = '' ;

		foreach ( $ml['data'] AS $k => $d ) {
			$d = $this->wd->extendContext ( $ml , $d ) ;
			if ( $d['type'] == 'sentence' ) {
				if ( $k > 0 ) $ret .= ' ' ;
				foreach ( $d['data'] AS $d2 ) {
					$d2 = $this->wd->extendContext ( $ml , $d2 ) ;
					$ret .= $this->renderML ( $d2 ) ;
				}
				$ret .= '.' ;
			} else if ( $d['type'] == 'subsentence' ) {
				if ( $k > 0 ) $ret .= ' ' ;
				foreach ( $d['data'] AS $d2 ) {
					$d2 = $this->wd->extendContext ( $ml , $d2 ) ;
					$ret .= $this->renderML ( $d2 ) ;
				}
				$ret .= ';' ;
			} else if ( $d['type'] == 'spo' or $d['type'] == 'po' ) {
			
				$lo = $this->getObjectDescription($d) ;

				if ( $d['predicate'] == 'P31' ) {
					if ( $d['type'] == 'spo' ) {
						$ret .= $this->wd->renderLink($d['subject']) ;
						if ( isset($ml['context']['past']) ) $ret .= ' was' ;
						else $ret .= ' is' ;
						$fc = strtolower ( substr ( 0 , 1 , trim ( $lo ) )) ;
						if ( in_array ( $fc , array('a','e','i','o','u') ) ) $ret .= ' an' ;
						else $ret .= ' a' ;
					}
					$ret .= " " . $lo ;
				} else {
					$ret .= $this->wd->getLabel($d['predicate']) ;
					$ret .= " " . $lo ;
				}
			
			} else {
				$ret .= "UNKNOWN TYPE " . $d['type'] ;
			}
		}
	
		return $ret ;
	}

}

// BEGIN CLASS WikidataDescription

class WikidataDescription {

	function WikidataDescription () {
		$this->items = array() ;
		$this->relations = array() ;
		$this->langs = array ( 'en' , 'de' , 'fr' , 'es' , 'it' , 'pl' , 'nl' ) ;
		$this->mode = 'text' ;
/*
		$this->langspec = array (
			'nationality' => array (
				'en'=>array(
					'Ecuador'=>'Ecuadorian','Ghana'=>'Ghanaian','Russia'=>'Russian','Argentina'=>'Argentine','Australia'=>'Australian','Austria'=>'Austrian','Belgium'=>'Belgian','Brazil'=>'Brazilian','Canada'=>'Canadian','Chile'=>'Chilean','China'=>'Chinese','Denmark'=>'Danish','Finland'=>'Finnish','Faroe Islands'=>'Faroese','Netherlands'=>'Dutch','Puerto Rico'=>'Puerto Rican','France'=>'French','Luxembourg'=>'Luxembourgish','Germany'=>'German','Greece'=>'Greek','Holland'=>'Dutch','Hungary'=>'Hungarian','Iceland'=>'Icelander','India'=>'Indian','Iran'=>'Iranian','Iraq'=>'Iraqi','Ireland'=>'Irish','Israel'=>'Israeli','Indonesia'=>'Indonesian','Italy'=>'Italian','Japan'=>'Japanese','Jamaica'=>'Jamaican','Jordan'=>'Jordanian','Mexico'=>'Mexican','Nepal'=>'Nepalese','New Zealand'=>'New Zealander','Norway'=>'Norwegian','Pakistan'=>'Pakistani','Paraguay'=>'Paraguayan','Peru'=>'Peruvian','Poland'=>'Polish','Romania'=>'Romanian','Scotland'=>'Scottish','South Africa'=>'South African','Spain'=>'Spanish','Switzerland'=>'Swiss','Syria'=>'Syrian','Thailand'=>'Thai','Turkey'=>'Turkish','USA'=>'American','Uruguay'=>'Uruguayan','Venezuela'=>'Venezuelan','Wales'=>'Welsh','United Kingdom'=>'British','United States of America'=>'US-American','Sweden'=>'Swedish'
				) ,
				'de'=>array(
					'Russland'=>'Russisch','Dänemark'=>'Dänisch','Norwegen'=>'Norwegisch','Niederlande'=>'Niederländisch','Deutschland'=>'Deutsch','Rumänien'=>'Rumänisch','Chile'=>'Chilenisch','Brasilien'=>'Brasilianisch'
				) ,
				'el'=>array(
					'Ελλάδα'=>'Έλληνας', 'Ρωσία'=>'Ρώσος','Δανία'=>'Δανός','Νορβηγία'=>'Νορβηγός','Ολλανδία'=>'Ολλανδός','Γερμανία'=>'Γερμανός', 'Χιλή'=>'Χιλιανός','Βραζιλία'=>'Βραζιλιάνος', 'Γαλλία'=>'Γάλλος', 'Αγγλία'=>'Άγγλος', 'Ηνωμένο Βασίλειο'=>'Βρετανός', 'Ηνωμένες Πολιτείες της Αμερικής'=>'Αμερικανός', 'Ισπανία'=>'Ισπανός', 'Ιταλία'=>'Ιταλός', 'Τουρκία'=>'Τούρκος', 'Βουλγαρία'=>'Βούλγαρος'
				)
			)
		) ;
*/
	}

	function init ( $root_item ) {
		foreach ( $this->langs AS $l ) {
			$class = 'WikidataDescription'.ucfirst($l) ;
			if ( !class_exists() ) continue ;
			$this->renderer = new $class ;
			break ;
		}
		if ( !isset($this->renderer) ) $this->renderer = new WikidataDescriptionEn ; // Fallback
		$this->renderer->wd = $this ;

		$this->loadQ ( $root_item ) ;
		$this->loadRelations ( $root_item ) ;
		$this->generateRelations() ;
	}

	function normQ ( $q ) {
		if ( !preg_match('/^P\d+$/',$q) ) $ret = preg_replace ( '/\D/' , '' , "$q" ) ;
		return "Q$ret" ;
	}

	function loadQ ( $qs ) {
		if ( !is_array($qs) ) $qs = array ( $qs ) ;
		foreach ( $qs AS $q ) { // TODO API/50-at-a-time faster?
			$q = $this->normQ($q) ;
			if ( isset ( $this->items[$q] ) ) continue ;
			$url = "http://www.wikidata.org/wiki/Special:EntityData/$q.json" ;
			$i = json_decode ( file_get_contents ( $url ) ) ; // TODO error handling
			$this->items[$q] = $i->entities->$q ;
		}
	}

	function getLabel ( $q ) {
		$q = $this->normQ($q) ;
		if ( !isset($this->items[$q]) ) return $q ; // Default label: Item
		if ( !isset($this->items[$q]->labels) ) return $q ; // Default label: Item
		foreach ( $this->langs AS $l ) {
			if ( !isset($this->items[$q]->labels->$l) ) continue ;
			return $this->items[$q]->labels->$l->value ;
		}
		foreach ( $this->items[$q]->labels AS $l => $v ) { // Just return the first one you can find
			return $v->value ;
		}
		return $q ; // Fallback
	}

	function generateRelations () {
		$nid = 'numeric-id' ;

		foreach ( $this->items AS $q => $i ) {
			if ( isset ( $i->claims ) ) {
				foreach ( $i->claims AS $p => $claims ) {
					foreach ( $claims AS $dummy => $claim ) {
						if ( !isset ( $claim->mainsnak ) ) continue ;
						if ( !isset ( $claim->mainsnak->datatype ) ) continue ;
						if ( $claim->mainsnak->datatype != 'wikibase-item' ) continue ;
						$q2 = $this->normQ ( $claim->mainsnak->datavalue->value->$nid.'' ) ;
						$this->relations[$q][$p][$q2] = $q2 ;
					}
				}
			}
		}
	
	}

	function loadRelations ( $q ) {
		$props = array() ;
	
		// Get target items for all claims
		if ( isset ( $this->items[$q]->claims ) ) {
			$nid = 'numeric-id' ;
			$targets = array() ;
			foreach ( $this->items[$q]->claims AS $p => $claims ) {
				$props[$p] = $p ;
				foreach ( $claims AS $dummy => $claim ) {
					if ( !isset ( $claim->mainsnak ) ) continue ;
					if ( !isset ( $claim->mainsnak->datatype ) ) continue ;
					if ( $claim->mainsnak->datatype != 'wikibase-item' ) continue ;
					$q2 = $this->normQ ( $claim->mainsnak->datavalue->value->$nid.'' ) ;
					$targets[$q2] = $q2 ;
				}
			}
			$this->loadQ ( $targets ) ;
		}
	
		$this->loadQ ( $props ) ;
	}


	function renderLink ( $q , $text ) {
		if ( !isset($text) ) $text = trim($this->getLabel($q)) ;
		if ( $text == '' ) $text = '('.$q.')' ;
		if ( $this->mode == 'text' ) return $text ;
		if ( $this->mode == 'wikidata' ) return "[[$q|$text]]" ;
		return "$text ($q)" ; // Fallback
	}

	function extendContext ( $ml , $d ) {
		if ( !isset($d['context']) ) $d['context'] = array() ;
		foreach ( $ml['context'] AS $k => $v ) {
			if ( !isset($d['context'][$k]) ) $d['context'][$k] = $v ;
		}
		return $d ;
	}






	function renderML ( $ml ) {
//		print_r ( $ml ) ;
		return $this->renderer->renderML ( $ml ) ;
	}




	function isHuman ( $q ) {
		if ( !isset($this->relations[$q]) ) return false ;
		if ( !isset($this->relations[$q]['P31']) ) return false ;
		if ( !isset($this->relations[$q]['P31']['Q5']) ) return false ;
		return true ;
	}

	function hasProperty ( $q , $p ) {
		return isset($this->items[$q]->claims->$p) ;
	}
	
	function addGenericList ( $q , &$root , $props ) {
		$ss1 = array ( 'type'=>'subsentence' , 'data'=>array() ) ;
		foreach ( $props AS $p ) {
			if ( !$this->hasProperty($q,"P$p") ) continue ;
			$p1 = array ( 'type'=>'po' ) ;
			$p1['predicate'] = "P$p" ;
			foreach ( $this->relations[$q]["P$p"] AS $k => $v ) $p1['object'][] = $v ;
			$ss1['data'][] = $p1 ;
		}
		if ( count($ss1['data']>0) ) {
			$root['data'][] = $ss1 ;
		}
	}

	function constructShortDescription ( $q ) {
	
		$ml = array ( 'context'=>array() , 'data'=>array() ) ;
	
		if ( $this->isHuman($q) ) {

			$s1 = array ( 'type'=>'sentence' , 'data'=>array() ) ;
			$ss1 = array ( 'type'=>'subsentence' , 'data'=>array() ) ;
			$p1 = array ( 'type'=>'spo' ) ;
			$p1['subject'] = $q ;
			$p1['predicate'] = 'P31' ;
			$p1['object'] = 'Q5' ;
			if ( $this->hasProperty($q,'P21') ) {
				foreach ( $this->relations[$q]['P21'] AS $k => $v ) $p1['object'] = $v ;
			}
			if ( $this->hasProperty($q,'P106') ) {
				$p1['object'] = array() ;
				foreach ( $this->relations[$q]['P106'] AS $k => $v ) $p1['object'][] = $v ;
			}
		
			if ( $this->hasProperty ($q,'P27') ) {
				foreach ( $this->relations[$q]['P27'] AS $k => $v ) $p1['attr'][] = array('p'=>'P27','q'=>$v) ;
			}
		
			$ss1['data'][] = $p1 ;
			$s1['data'][] = $ss1 ;
			$ml['data'][] = $s1 ;
		
			if ( $this->hasProperty($q,'P570') ) $ml['context']['past'] = true ; // Death date
		
		
		} else { // Generic
	
			// TODO: point in time, start date, end date, publication date
			
			$s1 = array ( 'type'=>'sentence' , 'data'=>array() ) ;
			
			$this->addGenericList ( $q , $s1 , array(279,31,60,105) ) ; // Instance, subclass etc.
			
			// TODO: Location
			
			$this->addGenericList ( $q , $s1 , array(175,86,170,57,50,61) ) ; // By
			


			$ml['data'][] = $s1 ;
	
		}
	
		return $this->renderML ( $ml ) ;
	}


}

// END OF CLASS


function endthis ( $status ) {
	global $test , $out ;
	if ( $status != '' ) $out['status'] = $status ;
	if ( $test ) {
		print_r ( $out ) ;
	} else {
		if ( isset($_REQUEST['callback']) ) print $_REQUEST['callback'] . "(" ;
		print json_encode ( $out ) ;
		if ( isset($_REQUEST['callback']) ) print ");" ;
	}
	exit ( 0 ) ;
}


$wd = new WikidataDescription ;

$out = array ( 'status' => 'OK' ) ;
$root_item = $wd->normQ ( get_request ( 'q' , '' ) ) ;
$wd->mode = get_request ( 'mode' , 'text' ) ;
$lang = get_request ( 'lang' , 'en' ) ;
array_unshift ( $wd->langs , $lang ) ;
if ( !isset($root_item) ) endthis ( "No valid item passed" ) ;


$out['q'] = $root_item ;
$out['language'] = $lang ;

$wd->init ( $root_item ) ;

$out['label'] = $wd->getLabel($root_item) ;

$out['short_desc'] = $wd->constructShortDescription ( $root_item ) ;

endthis() ;

?>