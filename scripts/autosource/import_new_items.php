#!/usr/bin/php
<?PHP

$db = new SQLite3 ( '/data/project/wikidata-todo/scripts/autosource/process.sqlite' ) ;
$items = explode ( "\n" , file_get_contents ( 'items2add.txt' ) ) ;
foreach ( $items AS $q ) {
	$q = preg_replace ( '/\D/' , '' , $q ) ;
	if ( $q == '' ) continue ;
	$sql = "INSERT OR IGNORE INTO items (q,done_with_version) VALUES ($q,0)" ;
	$db->exec ( $sql ) ;
}

?>