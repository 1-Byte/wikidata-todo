#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$people = explode ( "\n" , file_get_contents ( 'people2.tab' ) ) ;
$fh = fopen ( "people3.add" , 'w' ) ;
$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
$ref = "\tS143\tQ13679" ;

foreach ( $people AS $p ) {

	$sql = "SELECT count(*) AS cnt FROM wb_items_per_site WHERE ips_site_id='specieswiki' AND ips_site_page='" . $db->real_escape_string($p) . "'" ;
#print "$sql\n" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'." 1\n$sql\n\n");
	$skip = false ;
	if($o = $result->fetch_object()) {
		if ( $o->cnt > 0 ) $skip = true ;
	}
	if ( $skip ) continue ;


	$p2 = preg_replace ( '/\b[A-Z]\./' , ' ' , $p ) ;
	$p2 = trim ( preg_replace ( '/\s+/' , ' ' , $p2 ) ) ;
	$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srsearch=" . urlencode($p2) . "&format=json" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	if ( count($j->query->search) > 0 ) continue ;
	
	$s = "CREATE\n" ;
	$s .= "LAST\tSspecieswiki\t\"$p\"\n" ;
	$s .= "LAST\tLen\t\"$p\"\n" ;
	$s .= "LAST\tP31\tQ5$ref\n" ;
	

/*
	$url = "https://species.wikimedia.org/w/index.php?title=".myurlencode($p)."&action=raw" ;
	$page = file_get_contents ( $url ) ;
	$page = preg_replace ( '/\s+/' , ' ' , $page ) ;
	if ( !preg_match ( '/(\d{4})\s*[\-\–]\s*(\d{4})/u' , $page , $m ) ) continue ;
	$born = $m[1] ;
	$died = $m[2] ;
	
	$search = array() ;
	$p2 = preg_replace ( '/ [A-Z]\. /' , ' ' , $p ) ;
	$url = "https://www.wikidata.org/w/api.php?action=wbsearchentities&type=item&limit=50&search=" . urlencode($p2) . "&language=en&format=json" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	foreach ( $j->search AS $s ) $search[] = $s->id ;

	$url = "http://wdq.wmflabs.org/api?q=BETWEEN[569,$born-01-01,$born-12-32]%20AND%20BETWEEN[570,$died-01-01,$died-12-32]" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	
	$both = array() ;
	foreach ( $j->items AS $i ) {
		$q = "Q$i" ;
		if ( !in_array ( $q , $search ) ) continue ;
		$both[] = $q ;
	}
	
	if ( count($both) != 1 ) continue ;

	$q = $both[0] ;
	$s = "$q\tSspecieswiki\t\"$p\"\n" ;
*/
	fwrite ( $fh , $s ) ;
}

fclose ( $fh ) ;

?>