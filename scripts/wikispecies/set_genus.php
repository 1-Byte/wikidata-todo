#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$p225 = '225' ;
$species_list = array() ;
$query = 'claim[105:7432] and link[specieswiki] and noclaim[171]' ;
$url = "$wdq_internal_url?q=" . urlencode($query) . "&props=225" ;
$j = json_decode ( file_get_contents ( $url ) ) ;
foreach ( $j->props->$p225 AS $k => $v ) {
	if ( $v[1] == 'string' ) $species_list[$v[0]] = $v[2] ;
}

$geni = array() ;
$query = 'claim[105:34740]' ;
$url = "$wdq_internal_url?q=" . urlencode($query) . "&props=225" ;
$j = json_decode ( file_get_contents ( $url ) ) ;
foreach ( $j->props->$p225 AS $k => $v ) {
	if ( $v[1] == 'string' ) $geni[$v[2]] = $v[0] ;
}

$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
$dbsp = openDB ( 'en' , 'wikispecies' , true ) ;

$fh = fopen ( "species2genus.tab" , 'w' ) ;

foreach ( $species_list AS $q => $species ) {
	$spec = $db->real_escape_string ( ucfirst ( str_replace ( ' ' , '_' , trim ( $species ) ) ) ) ;
	$parts = explode ( '_' , $spec ) ;
	if ( count($parts) < 2 ) continue ; // Huh?
	$genus = $parts[0] ;
	$sql = "SELECT DISTINCT p2.page_title AS genus FROM templatelinks t1,page p1,templatelinks t2,page p2 WHERE t1.tl_from=p1.page_id AND p1.page_namespace=0 AND p1.page_title='$spec' AND t2.tl_from=p2.page_id AND p2.page_namespace=0 AND t1.tl_title=t2.tl_title AND t1.tl_title='$genus' and p2.page_title=t1.tl_title" ;
#print "$sql\n" ;
	if(!$result = $dbsp->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$ok = false ;
	while($o = $result->fetch_object()){
		if ( $o->genus == $genus ) $ok = true ;
	}
#print "$species\n1\n" ;
	if ( !$ok ) continue ;

#print "2\n" ;	
	if ( !isset($geni[$genus]) ) continue ;
#print "3\n" ;	
	
	$q_genus = false ;
	$sql = "select distinct ips_item_id from wb_items_per_site WHERE ips_site_id='specieswiki' AND ips_site_page='$genus'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	if($o = $result->fetch_object()) $q_genus = $o->ips_item_id ;
	if ( $q_genus === false ) continue ;
#print "4\n" ;	
	if ( $q_genus != $geni[$genus] ) continue ;
#print "5\n" ;	
	
	$s = "Q$q\tP171\tQ$q_genus\tS143\tQ13679" ;
	fwrite ( $fh , "$s\n" ) ;
}

fclose ( $fh ) ;

?>