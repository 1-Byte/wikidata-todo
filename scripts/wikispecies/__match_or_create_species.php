#!/usr/bin/php
<?PHP

# Gets Wikispecies-linked biographical Wikidata items, and tries to add some statements scraped from WS

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$db = openDBwiki ( 'specieswiki' , true ) ;

$t = file_get_contents ( 'https://tools.wmflabs.org/wikidata-todo/duplicity.php?wiki=specieswiki&mode=list' ) ;
$t = preg_replace ( '/<t.*?>/' , "\t" , $t ) ;
$t = preg_replace ( '/<.+?>/' , '' , $t ) ;
$rows = explode ( "\n" , $t ) ;
foreach ( $rows AS $r ) {
	if ( !preg_match ( '/^\s+[0-9,]+\t(.+?)\tcheck.+$/' , $r , $m ) ) continue ;
	$title = $m[1] ;
	if ( !preg_match ( '/^[A-Z][a-z]+ [a-z]+$/' , $title ) and !preg_match ( '/^[A-Z][a-z]+ [a-z]+ [a-z]+$/' , $title ) and !preg_match ( '/subsp\./' , $title ) ) continue ;
//	print "$title\n" ;
	
	$sparql = "SELECT ?q { ?q wdt:P225 '$title' }" ;
	$items = getSPARQLitems ( $sparql ) ;
	if ( count($items) == 0 ) {
		$found = 0 ;
		$sql = "SELECT * FROM page WHERE page_namespace=0 AND page_title='".str_replace(' ','_',$title)."' AND EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=0 AND pl_title IN ('Eukaryota','Bacteria'))" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()) $found = 1 ;
		if ( !$found ) continue ; // Not a species, probably
		print "CREATE\n" ;
		print "LAST\tSspecieswiki\t\"$title\"\n" ;
		print "LAST\tLen\t\"$title\"\n" ;
		print "LAST\tP225\t\"$title\"\n" ;
		print "LAST\tP31\tQ16521\n" ; // Taxon
		if ( preg_match ( '/^[A-Z][a-z]+ [a-z]+$/' , $title ) ) { // Species
			print "LAST\tP105\tQ7432\n" ;
		} else { // Subspecies
			print "LAST\tP105\tQ68947\n" ;
		}
	} else if ( count($items) == 1 ) {
		$q = 'Q'.$items[0] ;
		print "$q\tSspecieswiki\t\"$title\"\n" ;
	} else {
		// More than one taxon, skip
	}
}

?>