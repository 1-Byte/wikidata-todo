#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

ini_set('memory_limit','3500M');
set_time_limit ( 60 * 30 ) ; // 30min

require_once ( '/data/project/wikidata-todo/public_html/php/common.php' ) ;
require_once ( '/data/project/wikidata-todo/public_html/php/wikidata.php' ) ;

$testing = 0 ;

$filename = '/data/project/wikidata-todo/scripts/wikispecies/taxon_authors_and_dates.qs' ;
if ( file_exists ( $filename ) ) unlink ( $filename ) ;

$wil = new WikidataItemList() ;

$page2q = array() ;
if ( !$testing ) {// Items&sites with "taxon name" and "species" links
	$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
	$sql = "select page_title,ips_site_page from page,wb_entity_per_page,wb_items_per_site WHERE ips_site_id='specieswiki'" ;
	$sql .= " and epp_entity_type='item' and epp_entity_id=ips_item_id and page_id=epp_page_id and page_is_redirect=0" ;
	$sql .= " AND EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=120 AND pl_title='P225')" ; // "taxon name"
	$sql .= " AND EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=0 AND pl_title='Q7432')" ; // "species"
	$sql .= " AND NOT EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=120 AND pl_title IN ('P405','P574'))" ; // no author/date
//$sql .= " LIMIT 5000" ;
//	$r = rand() / getrandmax() ;
//	$sql .= " AND page_random >= $r ORDER BY page_random limit 1000" ;
	if(!$result = $db->query($sql)) die('1:There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) {
		$wspage = str_replace ( ' ' , '_' , $o->ips_site_page ) ;
		$page2q[$wspage] = $o->page_title ;
	}

	// Pages with author template
	// TODO: Just plain links to taxon authorities as well
	$pages = array() ;
	$db = openDB ( 'en' , 'wikispecies' , true ) ;
	$sql = "select page_title from page WHERE page_namespace=0 AND EXISTS (SELECT * FROM templatelinks WHERE page_id=tl_from AND tl_title IN ('A','Aut') AND tl_namespace=10)" ;
	if(!$result = $db->query($sql)) die('2:There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) {
		$pages[$o->page_title] = $o->page_title ;
	}
	$db->close() ;

	// Subset
	$keep = array() ;
	foreach ( $page2q AS $p => $q ) {
		if ( !isset($pages[$p]) ) continue ;
		$keep[$p] = $q ;
	}
	unset ( $pages ) ;

	if ( 1 ) { // Use random subset
		$page2q = array() ;
		$keys = array_rand ( $keep , 1000 ) ;
		foreach ( $keys AS $k ) $page2q[$k] = $keep[$k] ;
	} else {
		$page2q = $keep ;
	}
	unset ( $keep ) ;

} else { // TESTING
	$page = 'Acleris_tungurahuae' ;
	$q = $wil->loadItemByPage ( $page , 'specieswiki' ) ;
	$page2q = array ( $page => $q ) ;
}

//print "Processing " . count($page2q) . " candidate pages.\n" ;

$authorpage2q = array() ;

$out = array() ;
$wil->loadItems ( array_values ( $page2q ) ) ; // More efficient than one-at-a-time
foreach ( $page2q AS $page => $q ) {
	$i = $wil->getItem ( $q ) ;
	if ( !isset($i) ) continue ; // Paranoia
//	if ( $i->hasClaims('P405') ) continue ; // Has taxon author
//	if ( $i->hasClaims('P574') ) continue ; // Has date of taxon name publication
	$claims = $i->getClaims ( 'P225' ) ;
//	if ( isset($claims[0]->qualifiers) and count($claims[0]->qualifiers) > 0 ) continue ; // Already has qualifiers
	$taxon_name = $i->getFirstString ( 'P225' ) ;

	$base = "$q\tP225\t\"$taxon_name\"\t" ;

	
if ( $testing ) print "Trying $page / $q / $taxon_name\n" ;
	
	$url = "https://species.wikimedia.org/w/index.php?action=raw&title=" . myurlencode($page) ;
	$wikitext = file_get_contents ( $url ) ;
	if ( !isset($wikitext) ) continue ;
	$rows = explode ( "\n" , $wikitext ) ;
	$authors = array() ; // There should only be one
	foreach ( $rows AS $row ) {
		if ( !preg_match ( "/^\*{0,1}\s*''".$taxon_name."''\s*(.+)$/i" , $row , $m ) ) {
			if ( $taxon_name != str_replace('_',' ',$page) or !preg_match ( "/^\*{0,1}\s*''\{\{BASEPAGENAME\}\}''\s*(.+)$/i" , $row , $m ) ) {
				if ( $taxon_name != str_replace('_',' ',$page) or !preg_match ( "/^\*{0,1}\s*\{\{Spage\}\}\s*(.+)$/i" , $row , $m ) ) continue ;
			}
		}
		$row = $m[1] ; // Without leading taxon name

		// Parse year
		$year = '' ;
		if ( preg_match ( '/^(.+),{0,1}\s*(\d{4})\){0,1}\s*$/' , $row , $m ) ) {
			$row = $m[1] ;
			$year = $m[2] ;
		}
		
		// Remove leading/trailing junk
		$row = preg_replace ( '/^\s*\(\s*/' , '' , $row ) ; 
		$row = preg_replace ( '/\s*\)\s*$/' , '' , $row ) ; 
		$row = preg_replace ( '/\s*,\s*$/' , '' , $row ) ; 
		$row = trim ( $row ) ;

if ( $testing ) print "!!$row | $year\n" ;

		$na = array() ;
		if ( preg_match ( "/^\[\[(.+?)\|(.+?)\]\],{0,1}\s*(\d{4})$/" , $row , $m ) ) {
			$na[] = array ( 'page' => $m[1] , 'abbrev' => $m[2] , 'year' => $year ) ;
		} else if ( preg_match ( "/^\[\[(.+?)\]\]\s*[,\&]\s*\[\[(.+?)\]\]$/" , $row , $m ) ) {
			$ac = array ( $m[1] , $m[2] ) ;
			foreach ( $ac AS $a ) {
				if ( preg_match ( '/^(.+)\|(.+)$/' , $a , $m ) ) $na[] = array ( 'page' => trim($m[1]) , 'abbrev' => trim($m[1]) , 'year' => $year ) ;
				else $na[] = array ( 'page' => trim($a) , 'year' => $year ) ;
			}
		} else if ( preg_match ( "/^\[\[(.+?)\]\]$/" , $row , $m ) ) {
			$na[] = array ( 'page' => $m[1] , 'year' => $year ) ;
		} else if ( preg_match ( "/^\{\{(A|a|Aut|aut)\|(.+?)\}\}$/" , $row , $m ) ) { //
			$template = strtolower ( $m[1] ) ;
			$author = preg_replace ( '/[\[\]]/' , '' , $m[2] ) ;
			if ( preg_match ( '/^(.+)\|(.+)$/' , $author , $m ) ) $na[] = array ( 'page' => trim($m[1]) , 'abbrev' => trim($m[2]) , 'year' => $year ) ;
			else $na[] = array ( 'page' => trim($author) , 'year' => $year ) ;
			if ( $template == 'aut' ) $na[0]['nolink'] = true ;
		}
		
		if ( count($na) == 0 ) continue ;
		
		// Paranoia
		$ok = true ;
		foreach ( $na AS $a ) {
			if ( preg_match ( '/[\[\]\{\}\|]/' , $a['page']  ) ) $ok = false ;
		}
		
		if ( $ok ) $authors[] = $na ;
	}
	if ( count($authors) != 1 ) continue ;
	
	$quals = array() ;
	
	$found_authors = 0 ;
	foreach ( $authors[0] AS $author ) {
		$author = (object) $author ;
		if ( $author->nolink ) continue ;
		$author->page = str_replace ( ' ' , '_' , trim($author->page) ) ;

		$db = openDB ( 'en' , 'wikispecies' , true ) ;
		$sql = "select pl_title from page,pagelinks WHERE page_namespace=0 and page_title='".$db->real_escape_string($author->page)."' AND page_is_redirect=1 AND pl_from=page_id AND pl_namespace=0 LIMIT 1" ;
		if(!$result = $db->query($sql)) die('3:There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()) $author->page = $o->pl_title ;

		$author_q = '' ;
if ( $testing ) print "Checking author {$author->page}\n" ;
		if ( !isset ($authorpage2q[$author->page]) ) {
			$author_q = $wil->loadItemByPage ( $author->page , 'specieswiki' ) ;
			if ( $author_q === false ) continue ;
			$ia = $wil->getItem ( $author_q ) ;
			if ( !isset($ia) ) {
				print "Item lookup failed for {$author->page} / $author_q\n" ;
				continue ;
			}
			if ( !$ia->hasTarget ( 'P31' , 'Q5' ) ) { // Paranoia
				print "Not a human on WD: {$author->page} / $author_q\n" ;
				continue ;
			}
			$authorpage2q[$author->page] = $author_q ;
		} else {
			$author_q = $authorpage2q[$author->page] ;
		}
		$quals[] = "P405\t$author_q" ; // Taxon author
		$found_authors++ ;
//		$out[] = "$q\tP405\t$author_q" ; // Taxon author
	}
	
	if ( $found_authors != count($authors[0]) ) continue ; // Not all authors could be determined
	
	if ( isset ( $author->year ) and $author->year != '' ) {
		$quals[] = "P574\t+{$author->year}-01-01T00:00:00Z/9" ;
//		$out[] = "$q\tP574\t+{$author->year}-01-01T00:00:00Z/9" ;
	}
	
	if ( count($quals) == 0 ) continue ;
	
	$out[] = $base . implode ( "\t" , $quals ) ;
if ( $testing ) print "YUP\n" ;
}

$fh = fopen ( $filename , 'w' ) ;
$ref = "\tS143\tQ13679" ;
foreach ( $out AS $o ) {
	fwrite ( $fh , "$o$ref\n" ) ;
	if ( $testing ) print "$o$ref\n" ; // TESTING
}
fclose ( $fh ) ;

if ( !$testing ) `/data/project/wikidata-todo/scripts/quick_statements.php $filename` ;
