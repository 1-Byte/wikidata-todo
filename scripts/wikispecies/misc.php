#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;


# Virus
$dbsw = openDB ( 'en' , 'wikispecies' ) ;
$sql = "select * from page WHERE NOT EXISTS (SELECT * FROM page_props WHERE pp_page=page_id AND pp_propname='wikibase_item') AND page_title LIKE '%virus%' AND page_namespace=0 AND page_is_redirect=0" ;
if(!$result = $dbsw->query($sql)) die('There was an error running the query [' . $dbsw->error . ']'." 1\n$sql\n\n");
while($o = $result->fetch_object()) {
	$title = str_replace ( '_' , ' ' , $o->page_title ) ;
	if ( preg_match ( '/"/' , $title ) ) continue ;
	if ( !preg_match ( '/virus\b/' , $title ) ) continue ;
	$items = getSPARQLitems ( "SELECT ?q { ?q wdt:P225 '$title' OPTIONAL { ?article schema:about ?q ; schema:isPartOf <https://species.wikimedia.org/> } FILTER ( !bound(?article) ) }" ) ;
	if ( count($items) == 1 ) {
		$q = $items[0] ;
		print "Q$q\tSspecieswiki\t\"$title\"\n" ;
	} else {
		print "CREATE\n" ;
		print "LAST\tP31\tQ16521\n" ;
		print "LAST\tLen\t\"$title\"\n" ;
		print "LAST\tP225\t\"$title\"\n" ;
		print "LAST\tSspecieswiki\t\"$title\"\n" ;
	}
	
}

/*
# ISSN
$dbsw = openDB ( 'en' , 'wikispecies' ) ;
$sql = "select * from page WHERE NOT EXISTS (SELECT * FROM page_props WHERE pp_page=page_id AND pp_propname='wikibase_item') AND page_title LIKE 'ISSN_%' AND page_namespace=0 AND page_is_redirect=0" ;
if(!$result = $dbsw->query($sql)) die('There was an error running the query [' . $dbsw->error . ']'." 1\n$sql\n\n");
while($o = $result->fetch_object()) {
	$title = str_replace ( '_' , ' ' , $o->page_title ) ;
	if ( !preg_match ( '/^ISSN_(.+)$/' , $o->page_title , $m ) ) continue ;
	$issn = $m[1] ;
	$items = getSPARQLitems ( "SELECT ?q { ?q wdt:P236 '$issn' OPTIONAL { ?article schema:about ?q ; schema:isPartOf <https://species.wikimedia.org/> } FILTER ( !bound(?article) ) }" ) ;
	if ( count($items) == 1 ) {
		$q = $items[0] ;
		print "Q$q\tSspecieswiki\t\"$title\"\n" ;
	} else {
		print "CREATE\n" ;
		print "LAST\tLen\t\"ISSN $issn\"\n" ;
		print "LAST\tP236\t\"$issn\"\n" ;
		print "LAST\tSspecieswiki\t\"$title\"\n" ;
	}
}
*/

# Braces
$dbsw = openDB ( 'en' , 'wikispecies' ) ;
$sql = "select * from page WHERE NOT EXISTS (SELECT * FROM page_props WHERE pp_page=page_id AND pp_propname='wikibase_item') AND page_title LIKE '%(%' AND page_namespace=0 AND page_is_redirect=0" ;
$sql .= " AND EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=0 AND pl_title IN ('Bacteria','Eukaryota'))" ;
if(!$result = $dbsw->query($sql)) die('There was an error running the query [' . $dbsw->error . ']'." 1\n$sql\n\n");
while($o = $result->fetch_object()) {
	$title = str_replace ( '_' , ' ' , $o->page_title ) ;
	$t2 = trim ( preg_replace ( '/\s*\(.+?\)\s*/' , ' ' , $title ) ) ;
	print "CREATE\n" ;
	print "LAST\tLen\t\"$t2\"\n" ;
	print "LAST\tP31\tQ16521\n" ; # Taxon
	print "LAST\tSspecieswiki\t\"$title\"\n" ;
}

?>