#!/usr/bin/php
<?PHP

# Gets Wikispecies-linked biographical Wikidata items, and tries to add some statements scraped from WS

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;
require_once ( '../../public_html/php/wikidata.php' ) ;

$testing = 0 ;

$fh = '' ;
if ( $testing ) $fh = fopen ( "ws2item2.add" , 'w' ) ;
else $fh = fopen ( "ws2item.add" , 'w' ) ;

function formatDate ( $d ) {
	$ret = '' ;
	if ( preg_match ( '/^\d+$/' , $d ) ) $ret = "+$d-01-01T00:00:00Z/9" ;

	// Sanity checks
	if ( preg_match ( '/^\+(\d+)/' , $ret , $m ) ) {
		$year = $m[1] * 1 ;
		if ( $year < 1600 or $year > 2020 ) return '' ;
	}

	return $ret ;
}

$occs = array() ;

function getOccupationQ ( $s ) {
	global $occs ;
	$s = trim ( strtolower ( $s ) ) ;
	if ( isset ( $occs[$s] ) ) return $occs[$s] ;
	$sparql = "SELECT DISTINCT ?item WHERE { ?item wdt:P31 ?occ . ?occ wdt:P279* wd:Q28640 . ?item rdfs:label ?label FILTER ( str(?label)='$s')}" ;
	$qs = getSPARQLitems ( $sparql , 'item' ) ;
	$ret = '' ;
	if ( count($qs) == 1 ) $ret = $qs[0] ;
	$occs[$s] = $ret ;
	return $ret ;
}

$out_cache = array() ;
function out ( $s ) {
	global $out_cache , $fh ;
	if ( isset($out_cache[$s]) ) return ;
	$source = "\tS143\tQ13679" ;
	fwrite ( $fh , "$s$source\n" ) ;
	$out_cache[$s] = 1 ;
}

$regexes = array (
#	'/\[\[Category:Ichthyologists/i' => "P106\tQ4205432" ,
) ;

// Load nationalities
$nationalities = array () ;
$nat = getSPARQL ( "SELECT ?country ?name { ?country wdt:P31 wd:Q6256 . ?country wdt:P1549 ?name . FILTER ( lang(?name)='en') }" ) ;
foreach ( $nat->results->bindings AS $x ) {
	$q = preg_replace ( '/^.+\/Q/' , '' , $x->country->value ) ;
	$name = $x->name->value ;
	$nationalities[$name] = $q ;
}

$j = '' ;
if ( $testing ) { # Testing/manual
	$j = (object) array ( 'items' => array ( 21518000,21518001,21518002,21518003,21518004,21518006,21518007,21518008,21518009,21518011,21518012,21518014,21518015,21518016,21518017,21518018,21518019,21518021,21518022,21518023,21518024,21518025,21518027,21518028,21518029,21518030,21518031,21518032,21518034,21518036,21518037,21518038,21518039,21518041,21518042,21518043,21518044,21518045,21518046,21518047,21518049,21518050,21518051,21518052,21518055,21518056,21518057,21518058,21518059,21518061,21518062,21518063,21518064,21518065,21518066,21518067,21518069,21518070,21518071,21518072,21518074,21518075,21518076,21518077,21518078,21518080,21518081,21518082,21518083,21518084,21518085,21518087,21518088,21518089,21518090,21518091,21518092,21518094,21518095,21518096,21518097,21518098,21518101,21518102,21518103,21518104,21518105,21518107,21518108,21518109,21518110,21518111,21518112,21518114,21518115,21518116,21518117,21518118,21518120,21518121,21518122,21518123,21518124,21518126,21518127,21518128,21518129,21518131,21518133,21518135,21518136,21518137,21518138,21518139,21518140,21518142,21518143,21518144,21518145 ) ) ;
	#while ( count($j->items) > 1 ) array_shift ( $j->items ) ;
} else { # Real

	$j = (object) array ( 'items' => array () ) ;
	$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
	$sql = "select distinct ips_item_id from wb_entity_per_page,pagelinks,wb_items_per_site  WHERE ips_site_id='specieswiki' and epp_entity_id=ips_item_id and pl_from=epp_page_id AND epp_entity_type='item' AND pl_from_namespace=0 AND pl_namespace=0 AND pl_title='Q5'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $j->items[] = $o->ips_item_id ;
	

#	$j = (object) array ( 'items' => getSPARQLitems ( "SELECT ?item WHERE { ?item wdt:P31 wd:Q5 . ?article schema:about ?item . FILTER (SUBSTR(str(?article), 1, 35) = 'https://species.wikimedia.org/wiki/') }" , 'item' ) ) ;
#	$url = 'http://wdq.wmflabs.org/api?q=claim[31:5]%20and%20link[specieswiki]' ;
#	$j = json_decode ( file_get_contents ( $url ) ) ;
}
#print_r ( $j ) ;


$wil = new WikidataItemList ;
$wil->loadItems ( $j->items ) ;
foreach ( $j->items AS $q ) {
	if ( !$wil->hasItem($q) ) continue ;
	$i = $wil->getItem($q) ;
	$page = $i->getSitelink ( 'specieswiki' ) ;
	if ( !isset($page) ) continue ;

	$w = file_get_contents ( "https://species.wikimedia.org/w/index.php?title=" . myurlencode(str_replace(' ','_',$page)) . "&action=raw" ) ;
	$w = str_replace ( "\n" , ' !!! ' , $w ) ;

#	print "$page\n$w\n\n" ;

	// Human
	if ( !$i->hasClaims('P31') ) {
		out ( "Q$q\tP31\tQ5" ) ;
	}
	
	// Nationality
	if ( !$i->hasClaims('P27') ) {
		foreach ( $nationalities AS $rx => $cq ) {
			if ( !preg_match ( '/'.$rx.'/' , $w ) ) continue ;
			out ( "Q$q\tP27\tQ$cq" ) ;
			break ;
		}
	}
	
	// Occupation
	if ( preg_match ( '/\b(\S{3,}ist)\b/' , $w , $m ) ) {
		$oq = getOccupationQ ( $m[1] ) ;
		if ( $oq != '' and !$i->hasTarget('P106',$oq) ) out ( "Q$q\tP106\tQ$oq" ) ;
	}
	if ( preg_match ( '/\[\[Category:(\S{3,}ist)s\b/i' , $w , $m ) ) {
		$oq = getOccupationQ ( $m[1] ) ;
		if ( $oq != '' and !$i->hasTarget('P106',$oq) ) out ( "Q$q\tP106\tQ$oq" ) ;
	}
	
	// Fixed regexes
	foreach ( $regexes AS $r => $o ) {
		if ( !preg_match($r,$w) ) continue ;
		out ( "Q$q\t$o" ) ;
	}
	
	// Dates
	if ( !$i->hasClaims('P569') or !$i->hasClaims('P570') ) {
		$birth = '' ;
		$death = '' ;
		if ( preg_match ( '/\b(\d{4})\s*[—\-]\s*(\d{4})\b/' , $w , $m ) ) { $birth = $m[1] ; $death = $m[2] ; }
		else if ( preg_match ( '/\(\s*(\d{4})\s*[—\-]\s*\)/' , $w , $m ) ) { $birth = $m[1] ; }
		else if ( preg_match ( '/\bborn\s*(\d{4})\b/' , $w , $m ) ) { $birth = $m[1] ; }
		else if ( preg_match ( '/\(\s*[—\-]\s*(\d{4})\s*\)/' , $w , $m ) ) { $death = $m[1] ; }
		else if ( preg_match ( '/\bdied\s*(\d{4})\b/' , $w , $m ) ) { $death = $m[1] ; }
#print "$birth:$death\n" ;
		$birth = formatDate ( $birth ) ;
		$death = formatDate ( $death ) ;

		if ( $birth != '' and !$i->hasClaims('P569') ) out ( "Q$q\tP569\t$birth" ) ;
		if ( $death != '' and !$i->hasClaims('P570') ) out ( "Q$q\tP570\t$death" ) ;
	}
	
	// Botanic author ID
	if ( !$i->hasClaims('P428') and preg_match ( '/\[\[category:botanists/i' , $w ) ) {
		$key = '' ;
		if ( preg_match ( '/\b(form|abbreviation)\s*:\s*(.+?)\s*!/i' , $w , $m ) ) $key = $m[2] ;
		else if ( preg_match ( "/\('''(.+?)'''\)/" , $w , $m ) ) $key = $m[1] ;
		else if ( preg_match ( "/'''\((.+?)\)'''/" , $w , $m ) ) $key = $m[1] ;
		else if ( preg_match ( "/\('''\[\[(.+?)\]\]'''\)/" , $w , $m ) ) $key = $m[1] ;
		else if ( preg_match ( "/'''\(\[\[(.+?)\]\]\)'''/" , $w , $m ) ) $key = $m[1] ;
		else if ( preg_match ( "/'''([a-z\.]+?)'''/i" , $w , $m ) ) $key = $m[1] ; // Fallback

		$key = preg_replace ( '/\{\{aut\|/' , '' , $key ) ;
		$key = preg_replace ( '/\{\{a\|/' , '' , $key ) ;
#		$key = preg_replace ( '/[ \[\]\{\}]/' , '' , $key ) ;
		$key = preg_replace ( '/\'{2,}/' , '' , $key ) ;
		$key = preg_replace ( '/[\[\{]/' , '' , $key ) ;
		$key = preg_replace ( '/[\]\}].*$/' , '' , $key ) ;
		$key = preg_replace ( '/^\s+/' , '' , $key ) ;
		$key = preg_replace ( '/\s+$/' , '' , $key ) ;
		if ( $key != '' ) out ( "Q$q\tP428\t\"$key\"" ) ;
	}
	
}

fclose ( $fh ) ;

?>